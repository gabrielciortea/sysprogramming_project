﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributedServer
{
    class MessageToClient
    {
        ServerUtilities su = new ServerUtilities();

        public void Send_Client_All_Userdata(List<ConnectedClient> cc)
        {
            List<Player_Data_PDU> AUsers = new List<Player_Data_PDU>();

            foreach (ConnectedClient cClient in cc)
            {
                Player_Data_PDU U_Data = cClient.Player.UpdateAllPlayerDataServer();
                AUsers.Add(U_Data);
            }

            foreach (ConnectedClient c in cc)
            {
                foreach (Player_Data_PDU p in AUsers)
                {
                    if (c.SocketInUse == true)
                    {
                        try
                        {
                            System.Threading.Thread.Sleep(100);
                            byte[] PlayerDataPDU = su.Serialize(p);
                            c.ClientSocket.Send(PlayerDataPDU, SocketFlags.None);
                        }
                        catch (SocketException se)
                        {
                            MessageBox.Show("error:" + se);
                        }
                    }

                }

            }
        }

        public void Send_Client_ConnectionSignal(Socket _UdpServer, Connection_PDU ConMsg, int UdpPort)
        {
            IPEndPoint AllClients = new IPEndPoint(IPAddress.Broadcast, UdpPort);
            byte[] ServerConnectionPDU = su.Serialize(ConMsg);
            _UdpServer.EnableBroadcast = true;
            _UdpServer.SendTo(ServerConnectionPDU, AllClients);
        }

        public void Send_Client_All_NewMessage(List<ConnectedClient> cc, Player_Comm_PDU msg)
        {
            foreach (ConnectedClient cClient in cc)
            {
                if (cClient.SocketInUse == true)
                {
                    try
                    {
                        byte[] PlayerDataPDU = su.Serialize(msg);
                        System.Threading.Thread.Sleep(20);
                        cClient.ClientSocket.Send(PlayerDataPDU, SocketFlags.None);
                    }
                    catch (SocketException se)
                    {
                        MessageBox.Show("error:" + se);
                    }
                }
            }
        }

        public void Send_Client_Disable_Ball(List<ConnectedClient> cc, Player_Comm_PDU msg)
        {
            foreach (ConnectedClient cClient in cc)
            {
                if (cClient.SocketInUse == true)
                {
                    try
                    {
                        byte[] PlayerDataPDU = su.Serialize(msg);
                        System.Threading.Thread.Sleep(10);
                        cClient.ClientSocket.Send(PlayerDataPDU, SocketFlags.None);
                    }
                    catch (SocketException se)
                    {
                        MessageBox.Show("error:" + se);
                    }
                }
            }
        }

        public void Send_Client_Disable_Fence(Socket _TcpConnection, Player_Comm_PDU msg)
        {
            try
            {
                byte[] PlayerDataPDU = su.Serialize(msg);
                System.Threading.Thread.Sleep(20);
                _TcpConnection.Send(PlayerDataPDU, SocketFlags.None);
            }
            catch (SocketException se)
            {
                MessageBox.Show("error:" + se);
            }
        }

        public void Send_Client_All_StartGameAllowed(List<ConnectedClient> cc)
        {
            Player_Serv_PDU msg = new Player_Serv_PDU();
            msg.version = 0b00000001;
            msg.type = 0b00010001;

            foreach (ConnectedClient cClient in cc)
            {
                if (cClient.SocketInUse == true)
                {
                    try
                    {
                        byte[] PlayerDataPDU = su.Serialize(msg);
                        System.Threading.Thread.Sleep(30);
                        cClient.ClientSocket.Send(PlayerDataPDU, SocketFlags.None);
                    }
                    catch (SocketException se)
                    {
                        MessageBox.Show("error:" + se);
                    }
                }
            }
        }

        public void Send_Client_PlayerPosition(List<ConnectedClient> cc, Player_InGame_PDU P_PDU)
        {
            foreach (ConnectedClient cClient in cc)
            {
                if (cClient.SocketInUse == true)
                {
                    try
                    {
                        byte[] PlayerDataPDU = su.Serialize(P_PDU);
                        System.Threading.Thread.Sleep(20);
                        cClient.ClientSocket.Send(PlayerDataPDU, SocketFlags.None);
                    }
                    catch (SocketException se)
                    {
                        MessageBox.Show("error:" + se);
                    }
                }
            }
        }

        public void Send_All_Userdata_To_Rep_Server(List<ConnectedClient> cc, Socket _UdpServer, int UdpPort, string BkServer)
        {
            List<Player_Data_PDU> AUsers = new List<Player_Data_PDU>();

            foreach (ConnectedClient cClient in cc)
            {
                Player_Data_PDU U_Data = cClient.Player.UpdateAllPlayerDataServerToBackupServer();
                AUsers.Add(U_Data);
            }

            foreach (Player_Data_PDU p in AUsers)
            {
                try
                {
                    //System.Threading.Thread.Sleep(100);
                    IPAddress BS_IP = IPAddress.Parse(BkServer);
                    IPEndPoint BackupServer = new IPEndPoint(BS_IP, UdpPort);
                    byte[] ServerConnectionPDU = su.Serialize(p);
                    _UdpServer.SendTo(ServerConnectionPDU, BackupServer);
                }
                catch (SocketException se)
                {
                    MessageBox.Show("error:" + se);
                }

            }

        }

        public void Send_Username_Request(Socket _TcpConnection)
        {
            Player_Serv_PDU msg = new Player_Serv_PDU();
            msg.version = 0b00000001;
            msg.type = 0b11001111;
            try
            {
                byte[] ChatPDU = su.Serialize(msg);
                _TcpConnection.Send(ChatPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {

            }
        }

        public void Send_UserStartingCoordinates(Socket _TcpConnection, Player p)
        {
            Player_Data_PDU msg = new Player_Data_PDU();
            msg.version = 0b00000001;
            msg.type = 0b10000011;
            msg.username = p.Username;
            msg.posX = p.PosX;
            msg.posY = p.PosY;
            try
            {
                byte[] ChatPDU = su.Serialize(msg);
                _TcpConnection.Send(ChatPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {

            }
        }
    }
}
