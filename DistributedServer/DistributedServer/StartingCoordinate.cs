﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServer
{
    class StartingCoordinate
    {
        private struct Pos
        {
            private short x;
            private short y;
            private bool inUse;

            public short X { get => x; set => x = value; }
            public short Y { get => y; set => y = value; }
            public bool InUse { get => inUse; set => inUse = value; }
        }

        private Pos[] Positions = new Pos[2];

        public StartingCoordinate()
        {
            InitialisePositions();
        }

        public void InitialisePositions()
        {
            Positions[0].X = 25;
            Positions[0].Y = 485;
            Positions[0].InUse = false;
            Positions[1].X = 25;
            Positions[1].Y = 15;
            Positions[1].InUse = false;
        }

        public short[] GetNextAvailablePosition()
        {
            short[] pos = new short[2];

            for (int i = 0; i<2; i++)
            {
                if (Positions[i].InUse == false)
                {
                    pos[0] = Positions[i].X;
                    pos[1] = Positions[i].Y;
                    Positions[i].InUse = true;
                    break;
                }
            }
            return pos;
        }

        
    }
}
