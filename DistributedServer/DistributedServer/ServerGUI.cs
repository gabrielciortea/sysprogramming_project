﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Drawing;

namespace DistributedServer
{

    public partial class ServerGUI : Form
    {
        Socket _UdpServer;              // this socket is used by the server to update the clients
        Socket _UdpRepServerCheck;      // this socket is used by the primary server to check is there is a backup server
        Socket _UdpRepServerUpdate;     // this socket is used the the backup server to update the primary server
        Socket _TcpListener;            // this socket is used to accept new clients
        Socket[] _TempClient = new Socket[2];   // this 2 sockets are used by the backup server to accept connections temporarly

        System.Net.IPEndPoint localIPEndPoint;

        int TcpPort = 8001;
        int UdpPort = 8000;
        int UdpServerRepPort = 8002;
        int BkConnectedClients = 0;
        byte ServerVersion = 0b00000001;
        private const short MAXPLAYERS = 2;
        string Program;
        string PrimaryServer;
        string BackupServer;

        ServerUtilities su = new ServerUtilities();
        MessageToClient mtc = new MessageToClient();
        IPEndPoint _localIPEndPoint;

        private StartingCoordinate sc = new StartingCoordinate();
        bool StartAllowedSent = false;
        List<ConnectedClient> cc = new List<ConnectedClient>();
        List<string> LocalIPs;
        List<string> EventMessages = new List<string>();
       
        private static System.Windows.Forms.Timer Udp_ServerConnection;
        private static System.Windows.Forms.Timer Udp_CheckForReplication;
        private static System.Windows.Forms.Timer Udp_UpdateBackupServer;
        private static System.Windows.Forms.Timer Tcp_NewClientConnectionCheck;
        private static System.Windows.Forms.Timer Tcp_ConnectedClientCommunicationCheck;
        private static System.Windows.Forms.Timer Udp_CheckForPrimaryServer;
        private static System.Windows.Forms.Timer Udp_ServerUpdatesPeriodicCheck;
        private static System.Windows.Forms.Timer Tcp_BS_NewClientConnectionCheck;
        private static System.Windows.Forms.Timer Tcp_BS_ConnectedClientCommunicationCheck;

        public Connection_PDU ConMsg;

        public ServerGUI()
        {
            InitializeComponent();

            LocalIPs = su.GetLocalIPAddress_AsString(); // Get all the local IPs into a List of string
            StopServer_Button.Enabled = false;
            // Add the IPs to the combobox
            foreach (String s in LocalIPs)
            {
                LocalIP_ComboBox.Items.Add(s);
            }
            LocalIP_ComboBox.SelectedIndex = 0;

            // Initialise UDP connection socket and TCP listener
            try
            {
                _UdpServer = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                _UdpRepServerCheck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                _UdpRepServerUpdate = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                _TcpListener = su.InizializeTCPSocketNonBlocking(_TcpListener);
            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }

            InitialiseServerTimer();
        }

        private void UpdateServerConnectionPDU()
        {
            ConMsg.prg = Program;
            ConMsg.version = ServerVersion;
            ConMsg.ConnectedClients = (Int16)cc.Count;
            ConMsg.PrimaryServer = PrimaryServer;
            ConMsg.BackupServer = BackupServer;
        }

        private void InitialiseServerTimer()
        {
            Udp_ServerConnection = new System.Windows.Forms.Timer(); // initialise timer
            Udp_ServerConnection.Tick += new EventHandler(PS_Udp_ServerPeriodicUpdate); // Set event handler method for timer
            Udp_ServerConnection.Interval = 750;  // Timer interval is 0.75 s
            Udp_ServerConnection.Enabled = false;

            Udp_UpdateBackupServer = new System.Windows.Forms.Timer(); // initialise timer
            Udp_UpdateBackupServer.Tick += new EventHandler(PS_Udp_UpdateBackupServer); // Set event handler method for timer
            Udp_UpdateBackupServer.Interval = 500;  // Timer interval is 0.5 s
            Udp_UpdateBackupServer.Enabled = false;

            Udp_CheckForReplication = new System.Windows.Forms.Timer(); // initialise timer
            Udp_CheckForReplication.Tick += new EventHandler(PS_Udp_ReplicationPeriodicCheck); // Set event handler method for timer
            Udp_CheckForReplication.Interval = 400;  // Timer interval is 0.4 s
            Udp_CheckForReplication.Enabled = false;

            Tcp_NewClientConnectionCheck = new System.Windows.Forms.Timer(); // initialise timer
            Tcp_NewClientConnectionCheck.Tick += new EventHandler(PS_Tcp_NewClientPeriodicCheck); // Set event handler method for timer
            Tcp_NewClientConnectionCheck.Interval = 500;  // Timer interval is 0.7 s
            Tcp_NewClientConnectionCheck.Enabled = false;

            Tcp_ConnectedClientCommunicationCheck = new System.Windows.Forms.Timer(); // initialise timer
            Tcp_ConnectedClientCommunicationCheck.Tick += new EventHandler(PS_Tcp_ConnectedClientPeriodicCheck); // Set event handler method for timer
            Tcp_ConnectedClientCommunicationCheck.Interval = 30;  // Timer interval is 0.05 s
            Tcp_ConnectedClientCommunicationCheck.Enabled = false;

            // BK Server Timers
            Udp_CheckForPrimaryServer = new System.Windows.Forms.Timer(); // initialise timer
            Udp_CheckForPrimaryServer.Tick += new EventHandler(BS_Udp_PrimaryServerPeriodicCheck); // Set event handler method for timer
            Udp_CheckForPrimaryServer.Interval = 100;  // Timer interval is 0.1 s
            Udp_CheckForPrimaryServer.Enabled = false;

            Udp_ServerUpdatesPeriodicCheck = new System.Windows.Forms.Timer(); // initialise timer
            Udp_ServerUpdatesPeriodicCheck.Tick += new EventHandler(BS_Udp_ServerUpdatePeriodicCheck); // Set event handler method for timer
            Udp_ServerUpdatesPeriodicCheck.Interval = 200;  // Timer interval is 0.5 s
            Udp_ServerUpdatesPeriodicCheck.Enabled = false;

            Tcp_BS_NewClientConnectionCheck = new System.Windows.Forms.Timer(); // initialise timer
            Tcp_BS_NewClientConnectionCheck.Tick += new EventHandler(BS_Tcp_NewClientPeriodicCheck); // Set event handler method for timer
            Tcp_BS_NewClientConnectionCheck.Interval = 200;  // Timer interval is 0.1 s
            Tcp_BS_NewClientConnectionCheck.Enabled = false;

            Tcp_BS_ConnectedClientCommunicationCheck = new System.Windows.Forms.Timer(); // initialise timer
            Tcp_BS_ConnectedClientCommunicationCheck.Tick += new EventHandler(BS_Tcp_ConnectedClientPeriodicCheck); // Set event handler method for timer
            Tcp_BS_ConnectedClientCommunicationCheck.Interval = 30;  // Timer interval is 0.1 s
            Tcp_BS_ConnectedClientCommunicationCheck.Enabled = false;
        }

        private void StartServer_Button_Click(object sender, EventArgs e)
        {
            if (BackupServer_CheckBox.Checked == false)
            {
                Program = string.Format("PSGM" + '\0');
                PrimaryServer = LocalIP_ComboBox.SelectedItem.ToString();
                BackupServer = "127.0.0.1";
                try
                {
                    // Create an Endpoint that will cause the listening activity to apply to all the local node's interfaces
                    localIPEndPoint = new System.Net.IPEndPoint(IPAddress.Any, TcpPort);
                    _localIPEndPoint = new System.Net.IPEndPoint(IPAddress.Any, UdpServerRepPort);
                    // Bind to the local IP Address and selected port
                    _TcpListener.Bind(localIPEndPoint);
                    _UdpRepServerCheck.Blocking = false;
                    _UdpRepServerCheck.Bind(_localIPEndPoint);
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server binded to the port");
                }
                catch (SocketException se)
                {
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Bind not succeded. Reason: " + se.Message);
                }

                try
                {
                    _TcpListener.Listen(2); // Listen for connections, with a backlog / queue maximum of 2
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server is now listening and can accept connections");
                }
                catch (SocketException se)
                {
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server cannot listen the port. Reason: " + se.Message);
                }

                Udp_ServerConnection.Start();
                Udp_CheckForReplication.Start();
                Tcp_NewClientConnectionCheck.Start();
                StartServer_Button.Enabled = false;
                StopServer_Button.Enabled = true;
                LocalIP_ComboBox.Enabled = false;
                su.PrintEventLog(EventMessages, Events_RichTextBox);
            }
            else if(BackupServer_CheckBox.Checked == true)
            {
                Program = string.Format("BSGM" + '\0');
                PrimaryServer = "127.0.0.1";
                BackupServer = LocalIP_ComboBox.SelectedItem.ToString(); 
                try
                {
                    _localIPEndPoint = new System.Net.IPEndPoint(IPAddress.Any, UdpServerRepPort);
                    localIPEndPoint = new System.Net.IPEndPoint(IPAddress.Any, TcpPort);

                    _UdpRepServerUpdate.Bind(_localIPEndPoint);
                    _UdpRepServerUpdate.Blocking = false;
                    _TcpListener.Bind(localIPEndPoint);

                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Backup Server Started");
                    su.PrintEventLog(EventMessages, Events_RichTextBox);
                }
                catch (SocketException se)
                {
                    MessageBox.Show(se.Message);
                }
                try
                {
                    _TcpListener.Listen(2); // Listen for connections, with a backlog / queue maximum of 2
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server is now listening and can accept connections");
                }
                catch (SocketException se)
                {
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server cannot listen the port. Reason: " + se.Message);
                }
                Udp_CheckForPrimaryServer.Start();
                Tcp_BS_NewClientConnectionCheck.Start();
                StartServer_Button.Enabled = false;
                StopServer_Button.Enabled = true;
                LocalIP_ComboBox.Enabled = false;
            }
            
        }

        private void PS_Tcp_ConnectedClientPeriodicCheck(object sender, EventArgs e)
        {
            try
            {
                foreach (ConnectedClient cClient in cc)
                {
                    if (true == cClient.SocketInUse)
                    {
                        try
                        {
                            EndPoint localEndPoint = (EndPoint)localIPEndPoint;
                            byte[] ReceiveBuffer = new byte[1024];
                            int ReceivedByteCount;
                            ReceivedByteCount = cClient.ClientSocket.ReceiveFrom(ReceiveBuffer, ref localEndPoint);
                            byte[] PlayerPDU = new byte[ReceivedByteCount];
                            Array.Copy(ReceiveBuffer, PlayerPDU, ReceivedByteCount);

                            if (0 < ReceivedByteCount)
                            {
                                if (PlayerPDU[0] == 0b00000001) // then it is version 1
                                {
                                    if (PlayerPDU[1] == 0b00001111) // Updates All Data of a Player
                                    {
                                        Player_Data_PDU P_PDU = su.DeSerializePlayerDataPDU(PlayerPDU);
                                        cClient.Player.Username = P_PDU.username;
                                        cClient.Player.Victories = P_PDU.victories;
                                        cClient.Player.Colour = su.RandomColour();
                                        if ((cClient.Player.PosX == 0) && (cClient.Player.PosY == 0))
                                        {
                                            short[] coordinates = new short[2];
                                            coordinates = sc.GetNextAvailablePosition();
                                            cClient.Player.PosX = coordinates[0];
                                            cClient.Player.PosY = coordinates[1];
                                            mtc.Send_UserStartingCoordinates(cClient.ClientSocket, cClient.Player);
                                        }
                                        su.AddToListBox(cClient.Player.Username, ConnectedPlayers_ListBox);
                                        System.Threading.Thread.Sleep(100);
                                        mtc.Send_Client_All_Userdata(cc);
                                    }
                                    if (PlayerPDU[1] == 0b00000011) // Chat Message relayed to all players
                                    {
                                        Player_Comm_PDU P_PDU = su.DeSerializePlayerCommPDU(PlayerPDU);
                                        EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + P_PDU.msg);
                                        mtc.Send_Client_All_NewMessage(cc, P_PDU);
                                    }
                                    if (PlayerPDU[1] == 0b01010101) //Receives Disconnects Message From a Client
                                    {
                                        Player_Comm_PDU P_PDU = su.DeSerializePlayerCommPDU(PlayerPDU);
                                        EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + P_PDU.msg);
                                        su.DisconnectPlayer(cClient,cc);
                                        ConnectedPlayers_ListBox.Items.Remove((Object)P_PDU.username);
                                        mtc.Send_Client_All_NewMessage(cc, P_PDU);
                                    }
                                    if (PlayerPDU[1] == 0b11111111) // Winning signal
                                    {
                                        Player_Comm_PDU P_PDU = su.DeSerializePlayerCommPDU(PlayerPDU);
                                        EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + P_PDU.msg);
                                        cClient.Player.Victories++;
                                        mtc.Send_Client_All_NewMessage(cc, P_PDU);
                                        Player_Comm_PDU W_PUD = new Player_Comm_PDU();
                                        W_PUD.version = 0b0000001;
                                        W_PUD.type = 0b10100010;
                                        W_PUD.username = P_PDU.username;
                                        W_PUD.msg = " " + P_PDU.username + ": has now" + cClient.Player.Victories + " Victories";
                                        mtc.Send_Client_All_NewMessage(cc, W_PUD);
                                    }
                                    if (PlayerPDU[1] == 0b11111001) // Player fetched a ball
                                    {
                                        Player_Comm_PDU P_PDU = su.DeSerializePlayerCommPDU(PlayerPDU);
                                        mtc.Send_Client_Disable_Ball(cc, P_PDU);
                                    }
                                    if (PlayerPDU[1] == 0b11111000) // Player disabled a fence
                                    {
                                        Player_Comm_PDU P_PDU = su.DeSerializePlayerCommPDU(PlayerPDU);
                                        mtc.Send_Client_Disable_Fence(cClient.ClientSocket, P_PDU);
                                    }
                                    if (PlayerPDU[1] == 0b10001000) //A player has initialized and started the game
                                    {
                                        Player_Comm_PDU P_PDU = su.DeSerializePlayerCommPDU(PlayerPDU);
                                        System.Threading.Thread.Sleep(50);
                                        EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt ") + P_PDU.msg);
                                        mtc.Send_Client_All_NewMessage(cc, P_PDU);
                                    }
                                    if (PlayerPDU[1] == 0b11001100) // Update position in game
                                    {
                                        Player_InGame_PDU P_PDU = su.DeSerializePlayerInGamePDU(PlayerPDU);
                                        
                                        foreach (ConnectedClient c in cc)
                                        {
                                            if (P_PDU.username == c.Player.Username)
                                            {
                                                c.Player.PosX = P_PDU.posX;
                                                c.Player.PosY = P_PDU.posY;
                                            }
                                        }
                                        mtc.Send_Client_PlayerPosition(cc, P_PDU);
                                    }
                                }
                            }
                        }
                        catch (SocketException se) // Handle socket-related exception
                        {   // If an exception occurs, display an error message
                            if (10053 == se.ErrorCode || 10054 == se.ErrorCode) // Remote end closed the connection
                            {
                                //su.StopServer(connectedClients, serverListeningSocket);
                                StopServer_Button.Enabled = false;
                                StartServer_Button.Enabled = true;
                                //eventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server Stopped");
                            }
                            else if (10035 != se.ErrorCode)
                            {   // Ignore error messages relating to normal behaviour of non-blocking sockets
                                MessageBox.Show(se.Message);
                            }
                        }
                        catch // Silently handle any other exception
                        {
                        }
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": No clients connected");
            }
            su.PrintEventLog(EventMessages, Events_RichTextBox);
        }

        private void PS_Tcp_NewClientPeriodicCheck(object sender, EventArgs e)
        {
            if (cc.Count < MAXPLAYERS)
            {
                try
                {
                    cc.Add(new ConnectedClient(_TcpListener.Accept()));  // Accept a connection (if pending) and assign a new socket to it (AcceptSocket)
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": New player entered the room..");
                    su.PrintEventLog(EventMessages, Events_RichTextBox);
                    if ((cc.Count > 0) && (Tcp_ConnectedClientCommunicationCheck.Enabled == false))
                    {
                        Tcp_ConnectedClientCommunicationCheck.Enabled = true;
                        Tcp_ConnectedClientCommunicationCheck.Start();
                    }
                    UpdateServerConnectionPDU();
                }
                catch (SocketException se) // Handle socket-related exception
                {   // If an exception occurs, display an error message
                    if (10053 == se.ErrorCode || 10054 == se.ErrorCode) // Remote end closed the connection
                    {
                        //su.StopServer(connectedClients, serverListeningSocket);
                        StopServer_Button.Enabled = false;
                        StartServer_Button.Enabled = true;
                        EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server Stopped");
                        su.PrintEventLog(EventMessages, Events_RichTextBox);
                    }
                    else if (10035 != se.ErrorCode)
                    {   // Ignore error messages relating to normal behaviour of non-blocking sockets
                        EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Connection not established. Reason: " + se.Message);
                        su.PrintEventLog(EventMessages, Events_RichTextBox);
                    }
                    else
                    {
                    }
                }
                catch // Silently handle any other exception
                {
                    
                }
            }
            else
            {
                if (StartAllowedSent == false)
                {
                    mtc.Send_Client_All_StartGameAllowed(cc);
                    StartAllowedSent = true;
                }
                
            }
            
        }

        private void PS_Udp_ServerPeriodicUpdate(object sender, EventArgs e)
        {
            UpdateServerConnectionPDU();
            mtc.Send_Client_ConnectionSignal(_UdpServer, ConMsg, UdpPort);
            if (BackupServer == "127.0.0.1")
            {
                mtc.Send_Client_ConnectionSignal(_UdpServer, ConMsg, UdpServerRepPort);
            }
        }

        private void PS_Udp_UpdateBackupServer(object sender, EventArgs e)
        {
            if ((BackupServer != "127.0.0.1") && (cc.Count > 0) && (BackupServer_CheckBox.Checked == false))
            {
                mtc.Send_All_Userdata_To_Rep_Server(cc, _UdpServer, UdpServerRepPort, BackupServer);
            }
        }

        private void PS_Udp_ReplicationPeriodicCheck(object sender, EventArgs e)
        {
            try
            {
                EndPoint localEndPoint = (EndPoint)localIPEndPoint;
                byte[] ReceiveBuffer = new byte[1024];
                int iReceiveByteCount;
                iReceiveByteCount = _UdpRepServerCheck.ReceiveFrom(ReceiveBuffer, ref localEndPoint);
                byte[] ConnectionPDU = new byte[iReceiveByteCount];
                Array.Copy(ReceiveBuffer, ConnectionPDU, iReceiveByteCount);

                if (0 < iReceiveByteCount)
                {   // Copy the number of bytes received, from the message buffer to the text control
                    Connection_PDU ConPDU = su.DeSerialize(ConnectionPDU);
                    ServerVersion = ConPDU.version;

                    if ((ConPDU.prg == "BSGM") && (ServerVersion == 0b00000001))
                    {
                        BackupServer = ConPDU.BackupServer;
                        UpdateServerConnectionPDU();
                        mtc.Send_Client_ConnectionSignal(_UdpServer, ConMsg, UdpPort);
                        Udp_CheckForReplication.Stop();
                        Udp_UpdateBackupServer.Start();
                    }
                }
            }
            catch (SocketException se) // Catch any errors
            {   // Display a diagnostic message
                //EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": *** Server not found ***");
                //su.PrintEventLog(EventMessages, Events_RichTextBox);
            }

        }

        private void BS_Udp_PrimaryServerPeriodicCheck(object sender, EventArgs e)
        {
            try
            {
                EndPoint localEndPoint = (EndPoint)_localIPEndPoint;
                byte[] ReceiveBuffer = new byte[1024];
                int iReceiveByteCount;
                iReceiveByteCount = _UdpRepServerUpdate.ReceiveFrom(ReceiveBuffer, ref localEndPoint);
                byte[] ConnectionPDU = new byte[iReceiveByteCount];
                Array.Copy(ReceiveBuffer, ConnectionPDU, iReceiveByteCount);

                if (0 < iReceiveByteCount)
                {   // Copy the number of bytes received, from the message buffer to the text control
                    Connection_PDU ConPDU = su.DeSerialize(ConnectionPDU);
                    ServerVersion = ConPDU.version;

                    if ((ConPDU.prg == "PSGM") && (ServerVersion == 0b00000001) && (PrimaryServer == "127.0.0.1"))
                    {
                        EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": *** Connected to Primary Server ***");
                        su.PrintEventLog(EventMessages, Events_RichTextBox);
                        PrimaryServer = ConPDU.PrimaryServer;
                        UpdateServerConnectionPDU();
                        IPAddress PS_IP = IPAddress.Parse(ConPDU.PrimaryServer);
                        IPEndPoint _PSEndPoint = new IPEndPoint(PS_IP, UdpServerRepPort);
                        byte[] ServerConnectionPDU = su.Serialize(ConMsg);
                        _UdpRepServerUpdate.SendTo(ServerConnectionPDU, _PSEndPoint);
                        Udp_CheckForPrimaryServer.Stop();
                        Udp_ServerUpdatesPeriodicCheck.Start();
                    }
                }
            }
            catch (SocketException se) // Catch any errors
            {   // Display a diagnostic message
                EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": *** Primary Server not found ***");
                su.PrintEventLog(EventMessages, Events_RichTextBox);
            }
        }

        private void BS_Udp_ServerUpdatePeriodicCheck(object sender, EventArgs e)
        {
            try
            {
                EndPoint localEndPoint = (EndPoint)_localIPEndPoint;
                byte[] ReceiveBuffer = new byte[1024];
                int iReceiveByteCount;
                iReceiveByteCount = _UdpRepServerUpdate.ReceiveFrom(ReceiveBuffer, ref localEndPoint);
                byte[] PDU = new byte[iReceiveByteCount];
                Array.Copy(ReceiveBuffer, PDU, iReceiveByteCount);

                if (0 < iReceiveByteCount)
                {   // Copy the number of bytes received, from the message buffer to the text control
                    
                    if (PDU[0] == 0b00000001) // then it is version 1
                    {
                        if (PDU[1] == 0b01100110) // Receives data from primary server periodically
                        {
                            Player_Data_PDU P_PDU = su.DeSerializePlayerDataPDU(PDU);
                            if (cc.Count == 0 && (P_PDU.username != "")) 
                            {
                                cc.Add(new ConnectedClient(P_PDU.username,P_PDU.victories, P_PDU.posX, P_PDU.posY, P_PDU.colour));
                            }
                            else
                            {
                                bool userExists = false;
                                foreach (ConnectedClient cClient in cc)
                                {
                                    if ((cClient.Player.Username == P_PDU.username) && (P_PDU.username != ""))
                                    {
                                        userExists = true;
                                        cClient.Player.Victories = P_PDU.victories;
                                        cClient.Player.PosX = P_PDU.posX;
                                        cClient.Player.PosY = P_PDU.posY;
                                        cClient.Player.Colour = P_PDU.colour;
                                    }
                                }
                                if (userExists == false && (P_PDU.username != ""))
                                {
                                    cc.Add(new ConnectedClient(P_PDU.username, P_PDU.victories, P_PDU.posX, P_PDU.posY, P_PDU.colour));
                                }
                            }
                        }
                    }
                }
            }
            catch (SocketException se) // Catch any errors
            {   // Display a diagnostic message
                //EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": *** Error from PS ***");
                //su.PrintEventLog(EventMessages, Events_RichTextBox);
            }
        }
        
        private void BS_Tcp_NewClientPeriodicCheck(object sender, EventArgs e)
        {
            try
            {
                int SockAvailable = NextSocketAvailableArray();
                _TempClient[SockAvailable] = _TcpListener.Accept();  // Accept a connection (if pending) and assign a new socket to it (AcceptSocket)
                EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Primary Server has failed and a client connected "+ SockAvailable);
                su.PrintEventLog(EventMessages, Events_RichTextBox);
                UpdateServerConnectionPDU();
                mtc.Send_Username_Request(_TempClient[SockAvailable]);
                if (SockAvailable == 1)
                {
                    Tcp_BS_ConnectedClientCommunicationCheck.Enabled = true;
                    Tcp_BS_ConnectedClientCommunicationCheck.Start();
                    Udp_ServerConnection.Start();
                }
            }
            catch (SocketException se) // Handle socket-related exception
            {   // If an exception occurs, display an error message
                if (10053 == se.ErrorCode || 10054 == se.ErrorCode) // Remote end closed the connection
                {
                    //su.StopServer(connectedClients, serverListeningSocket);
                    StopServer_Button.Enabled = false;
                    StartServer_Button.Enabled = true;
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server Stopped");
                    su.PrintEventLog(EventMessages, Events_RichTextBox);
                }
                else if (10035 != se.ErrorCode)
                {   // Ignore error messages relating to normal behaviour of non-blocking sockets
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Connection not established. Reason: " + se.Message);
                    su.PrintEventLog(EventMessages, Events_RichTextBox);
                }
                else
                {
                }
            }
            catch // Silently handle any other exception
            {

            }
            
        }

        private void BS_Tcp_ConnectedClientPeriodicCheck(object sender, EventArgs e)
        {
            try
            {
                int i = 0;
                foreach (Socket s in _TempClient)
                {
                    try
                    {
                        EndPoint localEndPoint = (EndPoint)localIPEndPoint;
                        byte[] ReceiveBuffer = new byte[1024];
                        int ReceivedByteCount;
                        ReceivedByteCount = _TempClient[i].ReceiveFrom(ReceiveBuffer, ref localEndPoint);
                        byte[] PlayerPDU = new byte[ReceivedByteCount];
                        Array.Copy(ReceiveBuffer, PlayerPDU, ReceivedByteCount);

                        if (0 < ReceivedByteCount)
                        {
                            if (PlayerPDU[0] == 0b00000001) // then it is version 1
                            {
                                if (PlayerPDU[1] == 0b11001111) // Receives username of the player to match backup sockets to cc player class
                                {
                                    Player_Comm_PDU P_PDU = su.DeSerializePlayerCommPDU(PlayerPDU);
                                    foreach (ConnectedClient cClient in cc)
                                    {
                                        if (cClient.Player.Username == P_PDU.username)
                                        {
                                            su.AddToListBox(cClient.Player.Username, ConnectedPlayers_ListBox);
                                            cClient.ClientSocket = _TempClient[i];
                                            cClient.SocketInUse = true;
                                            BkConnectedClients++;
                                            if (BkConnectedClients == 2)
                                            {
                                                Tcp_BS_ConnectedClientCommunicationCheck.Stop();
                                                Tcp_ConnectedClientCommunicationCheck.Start();
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        i++;
                    }
                    catch
                    {

                    }
                }

            }
            catch
            {

            }
        }

        public int NextSocketAvailableArray()
        {
           if (_TempClient[0] == null)
            {
                return 0;
            }  
            else
            {
                return 1;
            }
        }

        private void StopServer_Button_Click(object sender, EventArgs e)
        {
            su.DisconnectAllPlayers(cc, _TcpListener);
            Udp_ServerConnection.Stop();
            Tcp_NewClientConnectionCheck.Stop();
            Tcp_ConnectedClientCommunicationCheck.Stop();
            StopServer_Button.Enabled = false;
            StartServer_Button.Enabled = true;
            LocalIP_ComboBox.Enabled = true;
        }
    }
}
