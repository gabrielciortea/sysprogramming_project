﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServer
{
    class Player
    {
        private string username;
        private short victories;
        private short posX;
        private short posY;
        private string colour;
        
        public Player(string u)
        {
            this.Username = u;
            this.PosX = 0;
            this.PosY = 0;
        }

        public Player(string u, short v, short posx, short posy, string colour)
        {
            this.Username = u;
            this.Victories = v;
            this.PosX = posx;
            this.PosY = posy;
            this.Colour = colour;
        }

        public string Username { get => username; set => username = value; }
        public short PosX { get => posX; set => posX = value; }
        public short PosY { get => posY; set => posY = value; }
        public short Victories { get => victories; set => victories = value; }
        public string Colour { get => colour; set => colour = value; }

        public Player_Data_PDU UpdateAllPlayerDataServer()
        {
            Player_Data_PDU p;
            p.version = 1;
            p.type = 0b00000111;
            p.username = this.username;
            p.victories = this.victories;
            p.colour = this.colour;
            p.posX = this.posX;
            p.posY = this.posY;
            return p;
        }

        public Player_Data_PDU UpdateAllPlayerDataServerToBackupServer()
        {
            Player_Data_PDU p;
            p.version = 1;
            p.type = 0b01100110;
            p.username = this.username;
            p.victories = this.victories;
            p.colour = this.colour;
            p.posX = this.posX;
            p.posY = this.posY;
            return p;
        }

        public Player_InGame_PDU UpdatePlayerStartingPosition(short[] c)
        {
            Player_InGame_PDU p;
            p.version = 1;
            p.type = 0b01000000;
            p.username = this.username;
            p.posX = c[0];
            p.posY = c[1];
            return p;
        }

    }
}
