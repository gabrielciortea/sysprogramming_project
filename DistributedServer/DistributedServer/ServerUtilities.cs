﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DistributedServer
{
    class ServerUtilities
    {
        bool RedInuse = false;
        bool BlueInuse = false;

        public List<string> GetLocalIPAddress_AsString()
        {
            List<string> allIPs = new List<string>();
            allIPs.Add("127.0.0.1");  // Default is local loopback address
            IPHostEntry IPHost = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress IP in IPHost.AddressList)
            {
                if (IP.AddressFamily == AddressFamily.InterNetwork) // Match only the IPv4 address
                {
                    allIPs.Add(IP.ToString());
                    break;
                }
            }
            return allIPs;
        }

        public Socket InizializeTCPSocketNonBlocking(Socket t)
        {
            try
            {   // Create the Listen socket, for TCP use
                t = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                t.Blocking = false;
            }
            catch (SocketException se)
            {
                MessageBox.Show("Socket Initialisation Error");
            }
            return t;
        }

        public void PrintEventLog(List<string> l, RichTextBox rtb)
        {
            string allEvents = "";
            foreach (string s in l)
            {
                allEvents = allEvents + s + "\n";
            }
            rtb.Text = allEvents;
        }

        public void AddToListBox(string t, ListBox ls)
        {
            bool exists = false;
            foreach (string s in ls.Items)
            {
                if (s == t)
                {
                    exists = true;
                }
            }
            if (exists == false)
            {
                ls.Items.Add(t);
            }
        }

        public string RandomColour()
        {
            string colour = "White";
            int b;
            Random r = new Random();
            b = r.Next(1, 5);
            switch (b)
            {
                case 1:
                    if (RedInuse == false)
                    {
                        colour = "Red";
                        RedInuse = true;
                    }
                    else
                    {
                        colour = "Blue";
                    }
                    break;
                case 2:
                    if (BlueInuse == false)
                    {
                        colour = "Blue";
                        BlueInuse = true;
                    }
                    else
                    {
                        colour = "Red";
                    }
                    break;
            }

            return colour;
        }
        // SERVER MAINTENANCE
        public void DisconnectAllPlayers(List<ConnectedClient> cConnected, Socket lSocket)
        {
            try
            {
                foreach (ConnectedClient cc in cConnected)
                {
                    cc.ClientSocket.Shutdown(SocketShutdown.Both);
                    cc.ClientSocket.Close();
                }
                lSocket.Close();
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void DisconnectPlayer(ConnectedClient cc, List<ConnectedClient> connectedClients)
        {
            try
            {
                cc.SocketInUse = false;
                cc.ClientSocket.Shutdown(SocketShutdown.Both);
                cc.ClientSocket.Close();
                connectedClients.Remove(cc);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        //converts a byte[] to a Message_PDU structure
        public Connection_PDU DeSerialize(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Connection_PDU structure = (Connection_PDU)Marshal.PtrToStructure(ip, typeof(Connection_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        //converts any object into a byte[]
        public byte[] Serialize(Object myObject)
        {
            int size = Marshal.SizeOf(myObject);
            IntPtr ip = Marshal.AllocHGlobal(size); //allocate unmanaged memory equivelent to the size of the object
            Marshal.StructureToPtr(myObject, ip, false); //marshal the object into the allocated memory
            byte[] byteArray = new byte[size];
            Marshal.Copy(ip, byteArray, 0, size); //place the contents of memory into a byte[]
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return byteArray;
        }

        //converts a byte[] to a Message_PDU structure
        public Connection_PDU DeSerializeConnectionPDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Connection_PDU structure = (Connection_PDU)Marshal.PtrToStructure(ip, typeof(Connection_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        //converts a byte[] to a Player_Data_PDU structure
        public Player_Data_PDU DeSerializePlayerDataPDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Player_Data_PDU structure = (Player_Data_PDU)Marshal.PtrToStructure(ip, typeof(Player_Data_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        //converts a byte[] to a Player_InGame_PDU structure
        public Player_InGame_PDU DeSerializePlayerInGamePDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Player_InGame_PDU structure = (Player_InGame_PDU)Marshal.PtrToStructure(ip, typeof(Player_InGame_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        public Player_Comm_PDU DeSerializePlayerCommPDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Player_Comm_PDU structure = (Player_Comm_PDU)Marshal.PtrToStructure(ip, typeof(Player_Comm_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        public Player_Serv_PDU DeSerializePlayerServPDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Player_Serv_PDU structure = (Player_Serv_PDU)Marshal.PtrToStructure(ip, typeof(Player_Serv_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }
    }
}
