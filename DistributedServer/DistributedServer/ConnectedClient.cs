﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServer
{
    class ConnectedClient
    {
        private Socket clientSocket;
        private bool socketInUse;
        private Player player;

        public bool SocketInUse { get => socketInUse; set => socketInUse = value; }
        public Socket ClientSocket { get => clientSocket; set => clientSocket = value; }
        internal Player Player { get => player; set => player = value; }

        public ConnectedClient()
        {
            this.ClientSocket = null;
            this.ClientSocket.Blocking = false;
            this.SocketInUse = false;
            this.Player = new Player("");
        }

        public ConnectedClient(Socket s)
        {
            this.ClientSocket = s;
            this.ClientSocket.Blocking = false;
            this.SocketInUse = true;
            this.Player = new Player("");
        }

        public ConnectedClient(string u)
        {
            this.ClientSocket = null;
            this.ClientSocket.Blocking = false;
            this.SocketInUse = false;
            this.Player = new Player(u);
        }

        public ConnectedClient(string u, short v, short posx, short posy, string colour)
        {
            this.ClientSocket = null;
            this.SocketInUse = false;
            this.Player = new Player(u,v,posx,posy,colour);
        }
    }
}
