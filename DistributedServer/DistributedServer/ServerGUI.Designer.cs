﻿namespace DistributedServer
{
    partial class ServerGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartServer_Button = new System.Windows.Forms.Button();
            this.LocalIP_ComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.StopServer_Button = new System.Windows.Forms.Button();
            this.Events_RichTextBox = new System.Windows.Forms.RichTextBox();
            this.Connection_GroupBox = new System.Windows.Forms.GroupBox();
            this.BackupServer_CheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ConnectedPlayers_ListBox = new System.Windows.Forms.ListBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.Connection_GroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StartServer_Button
            // 
            this.StartServer_Button.Location = new System.Drawing.Point(6, 79);
            this.StartServer_Button.Name = "StartServer_Button";
            this.StartServer_Button.Size = new System.Drawing.Size(107, 24);
            this.StartServer_Button.TabIndex = 0;
            this.StartServer_Button.Text = "Start Server";
            this.StartServer_Button.UseVisualStyleBackColor = true;
            this.StartServer_Button.Click += new System.EventHandler(this.StartServer_Button_Click);
            // 
            // LocalIP_ComboBox
            // 
            this.LocalIP_ComboBox.FormattingEnabled = true;
            this.LocalIP_ComboBox.Location = new System.Drawing.Point(82, 22);
            this.LocalIP_ComboBox.Name = "LocalIP_ComboBox";
            this.LocalIP_ComboBox.Size = new System.Drawing.Size(144, 24);
            this.LocalIP_ComboBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Server IP:";
            // 
            // StopServer_Button
            // 
            this.StopServer_Button.Location = new System.Drawing.Point(119, 79);
            this.StopServer_Button.Name = "StopServer_Button";
            this.StopServer_Button.Size = new System.Drawing.Size(107, 24);
            this.StopServer_Button.TabIndex = 4;
            this.StopServer_Button.Text = "Stop Server";
            this.StopServer_Button.UseVisualStyleBackColor = true;
            this.StopServer_Button.Click += new System.EventHandler(this.StopServer_Button_Click);
            // 
            // Events_RichTextBox
            // 
            this.Events_RichTextBox.Location = new System.Drawing.Point(1, 124);
            this.Events_RichTextBox.Name = "Events_RichTextBox";
            this.Events_RichTextBox.ReadOnly = true;
            this.Events_RichTextBox.Size = new System.Drawing.Size(521, 516);
            this.Events_RichTextBox.TabIndex = 5;
            this.Events_RichTextBox.Text = "";
            // 
            // Connection_GroupBox
            // 
            this.Connection_GroupBox.Controls.Add(this.BackupServer_CheckBox);
            this.Connection_GroupBox.Controls.Add(this.label1);
            this.Connection_GroupBox.Controls.Add(this.LocalIP_ComboBox);
            this.Connection_GroupBox.Controls.Add(this.StopServer_Button);
            this.Connection_GroupBox.Controls.Add(this.StartServer_Button);
            this.Connection_GroupBox.Location = new System.Drawing.Point(12, 12);
            this.Connection_GroupBox.Name = "Connection_GroupBox";
            this.Connection_GroupBox.Size = new System.Drawing.Size(235, 109);
            this.Connection_GroupBox.TabIndex = 6;
            this.Connection_GroupBox.TabStop = false;
            this.Connection_GroupBox.Text = "Connection";
            // 
            // BackupServer_CheckBox
            // 
            this.BackupServer_CheckBox.AutoSize = true;
            this.BackupServer_CheckBox.Location = new System.Drawing.Point(7, 52);
            this.BackupServer_CheckBox.Name = "BackupServer_CheckBox";
            this.BackupServer_CheckBox.Size = new System.Drawing.Size(123, 21);
            this.BackupServer_CheckBox.TabIndex = 8;
            this.BackupServer_CheckBox.Text = "Backup Server";
            this.BackupServer_CheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ConnectedPlayers_ListBox);
            this.groupBox1.Location = new System.Drawing.Point(276, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(235, 109);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connected Players";
            // 
            // ConnectedPlayers_ListBox
            // 
            this.ConnectedPlayers_ListBox.FormattingEnabled = true;
            this.ConnectedPlayers_ListBox.ItemHeight = 16;
            this.ConnectedPlayers_ListBox.Location = new System.Drawing.Point(7, 22);
            this.ConnectedPlayers_ListBox.Name = "ConnectedPlayers_ListBox";
            this.ConnectedPlayers_ListBox.Size = new System.Drawing.Size(219, 84);
            this.ConnectedPlayers_ListBox.TabIndex = 0;
            // 
            // ServerGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 641);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Connection_GroupBox);
            this.Controls.Add(this.Events_RichTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ServerGUI";
            this.Text = "GM Server";
            this.Connection_GroupBox.ResumeLayout(false);
            this.Connection_GroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StartServer_Button;
        private System.Windows.Forms.ComboBox LocalIP_ComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button StopServer_Button;
        private System.Windows.Forms.RichTextBox Events_RichTextBox;
        private System.Windows.Forms.GroupBox Connection_GroupBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox ConnectedPlayers_ListBox;
        private System.Windows.Forms.CheckBox BackupServer_CheckBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

