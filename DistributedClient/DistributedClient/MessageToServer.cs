﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DistributedClient
{
    class MessageToServer
    {
        ClientUtilities cu = new ClientUtilities();

        public void Tcp_SendPlayerUpdatePDU(Player p, Socket _TcpConnection)
        {
            Player_Data_PDU PPDU;
            PPDU = p.UpdateAllPlayerDataServer();

            try
            {
                byte[] PlayerPDU = cu.Serialize(PPDU);
                _TcpConnection.Send(PlayerPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void Tcp_SendPlayerUpdatePDUForBackupServer(Player p, Socket _TcpConnection)
        {
            Player_Comm_PDU PPDU;
            PPDU = p.GenerateUsernameForBackupServer();

            try
            {
                byte[] PlayerPDU = cu.Serialize(PPDU);
                _TcpConnection.Send(PlayerPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void Tcp_SendBallFetch(Player p, Socket _TcpConnection, string ball)
        {

            Player_Comm_PDU PPDU;
            PPDU = p.GenerateBallFetched(ball);

            try
            {
                byte[] PlayerPDU = cu.Serialize(PPDU);
                System.Threading.Thread.Sleep(20);
                _TcpConnection.Send(PlayerPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void Tcp_SendDisableFence(Player p, Socket _TcpConnection)
        {

            Player_Comm_PDU PPDU;
            PPDU = p.GenerateDisableFence();

            try
            {
                byte[] PlayerPDU = cu.Serialize(PPDU);
                _TcpConnection.Send(PlayerPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void Tcp_SendChatMessage(Player p,string msg, Socket _TcpConnection)
        {
            Player_Comm_PDU CPDU;
            CPDU = p.GenerateChatMessage(msg);
            try
            {
                byte[] ChatPDU = cu.Serialize(CPDU);
                _TcpConnection.Send(ChatPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void Tcp_SendStartGameSignal(Player p, string msg, Socket _TcpConnection)
        {
            Player_Comm_PDU CPDU;
            CPDU = p.GenerateStartSignal(msg);
            try
            {
                byte[] ChatPDU = cu.Serialize(CPDU);
                _TcpConnection.Send(ChatPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void Tcp_SendWinningSignal(Player p, string msg, Socket _TcpConnection)
        {
            Player_Comm_PDU CPDU;
            CPDU = p.GenerateWinningSignal(msg);
            try
            {
                byte[] ChatPDU = cu.Serialize(CPDU);
                _TcpConnection.Send(ChatPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void Tcp_SendInGameUpdate(Player p, Socket _TcpConnection)
        {
            Player_InGame_PDU CPDU;
            CPDU = p.GenerateUpdatePositionInGame(p);
            try
            {
                byte[] ChatPDU = cu.Serialize(CPDU);
                _TcpConnection.Send(ChatPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }

        public void Tcp_SendDisconnectMessage(Player p, Socket _TcpConnection)
        {
            Player_Comm_PDU CPDU;
            CPDU = p.GenerateDisconnectMessage(" " + p.Username + "Disconnected");
            try
            {
                byte[] ChatPDU = cu.Serialize(CPDU);
                _TcpConnection.Send(ChatPDU, SocketFlags.None);
            }
            catch // Silently handle any exceptions
            {
            }
        }
    }
}
