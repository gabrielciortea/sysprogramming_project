﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedClient
{
    public class Player
    {
        private string username;
        private short victories;
        private short posX;
        private short posY;
        private string colour;

        public Player(string u)
        {
            this.Username = u;
            this.Victories = 0;
            this.PosX = 0;
            this.PosY = 0;
            this.Colour = "";
        }

        public string Username { get => username; set => username = value; }
        public short PosX { get => posX; set => posX = value; }
        public short PosY { get => posY; set => posY = value; }
        public short Victories { get => victories; set => victories = value; }
        public string Colour { get => colour; set => colour = value; }

        public Player_Data_PDU UpdateAllPlayerDataServer()
        {
            Player_Data_PDU p;
            p.version = 0b0000001;
            p.type = 0b00001111;
            p.username = this.username;
            p.victories = this.victories;
            p.colour = this.colour;
            p.posX = this.posX;
            p.posY = this.posY;
            return p; 
        }

        public Player_Comm_PDU GenerateChatMessage(string m)
        {
            Player_Comm_PDU p;
            p.version = 0b0000001;
            p.type = 0b0000011;
            p.username = this.username;
            p.msg = " " + this.username + ": "+ m;
            return p;
        }

        public Player_Comm_PDU GenerateUsernameForBackupServer()
        {
            Player_Comm_PDU p;
            p.version = 0b0000001;
            p.type = 0b11001111;
            p.username = this.username;
            p.msg = "IamBack!";
            return p;
        }

        public Player_Comm_PDU GenerateStartSignal(string m)
        {
            Player_Comm_PDU p;
            p.version = 0b0000001;
            p.type = 0b10001000;
            p.username = this.username;
            p.msg = " " + this.username +": " + m;
            return p;
        }

        public Player_Comm_PDU GenerateWinningSignal(string m)
        {
            Player_Comm_PDU p;
            p.version = 0b0000001;
            p.type = 0b11111111;
            p.username = this.username;
            p.msg = this.username + ": " + m;
            return p;
        }

        public Player_Comm_PDU GenerateBallFetched(string m)
        {
            Player_Comm_PDU p;
            p.version = 0b0000001;
            p.type = 0b11111001;
            p.username = this.username;
            p.msg = m;
            return p;
        }

        public Player_Comm_PDU GenerateDisableFence()
        {
            Player_Comm_PDU p;
            p.version = 0b0000001;
            p.type = 0b11111000;
            p.username = this.username;
            p.msg = "Fence_PictureBox";
            return p;
        }

        public Player_Comm_PDU GenerateDisconnectMessage(string m)
        {
            Player_Comm_PDU p;
            p.version = 0b0000001;
            p.type = 0b01010101;
            p.username = this.username;
            p.msg = m;
            return p;
        }

        public Player_InGame_PDU GenerateUpdatePositionInGame(Player LocalPlayer)
        {
            Player_InGame_PDU p;
            p.version = 0b0000001;
            p.type = 0b11001100;
            p.username = LocalPlayer.username;
            p.posX = LocalPlayer.PosX;
            p.posY = LocalPlayer.PosY;
            return p;
        }

    }
}
