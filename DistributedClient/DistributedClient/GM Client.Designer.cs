﻿namespace DistributedClient
{
    partial class GMClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Events_RichTextBox = new System.Windows.Forms.RichTextBox();
            this.IPList_ComboBox = new System.Windows.Forms.ComboBox();
            this.StartClient_Button = new System.Windows.Forms.Button();
            this.StopClient_Button = new System.Windows.Forms.Button();
            this.Connection_GroupBox = new System.Windows.Forms.GroupBox();
            this.Username_TextBox = new System.Windows.Forms.TextBox();
            this.Username_Label = new System.Windows.Forms.Label();
            this.LocalIP_Label = new System.Windows.Forms.Label();
            this.ConnectedPlayers_ListBox = new System.Windows.Forms.ListBox();
            this.StartGame_Button = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Chat_TextBox = new System.Windows.Forms.TextBox();
            this.ChatSend_Button = new System.Windows.Forms.Button();
            this.Connection_GroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Events_RichTextBox
            // 
            this.Events_RichTextBox.Location = new System.Drawing.Point(-1, 175);
            this.Events_RichTextBox.Name = "Events_RichTextBox";
            this.Events_RichTextBox.ReadOnly = true;
            this.Events_RichTextBox.Size = new System.Drawing.Size(400, 407);
            this.Events_RichTextBox.TabIndex = 1;
            this.Events_RichTextBox.Text = "";
            // 
            // IPList_ComboBox
            // 
            this.IPList_ComboBox.FormattingEnabled = true;
            this.IPList_ComboBox.Location = new System.Drawing.Point(81, 21);
            this.IPList_ComboBox.Name = "IPList_ComboBox";
            this.IPList_ComboBox.Size = new System.Drawing.Size(132, 24);
            this.IPList_ComboBox.TabIndex = 2;
            // 
            // StartClient_Button
            // 
            this.StartClient_Button.Location = new System.Drawing.Point(4, 88);
            this.StartClient_Button.Name = "StartClient_Button";
            this.StartClient_Button.Size = new System.Drawing.Size(100, 29);
            this.StartClient_Button.TabIndex = 3;
            this.StartClient_Button.Text = "Start Client";
            this.StartClient_Button.UseVisualStyleBackColor = true;
            this.StartClient_Button.Click += new System.EventHandler(this.StartClient_Button_Click);
            // 
            // StopClient_Button
            // 
            this.StopClient_Button.Location = new System.Drawing.Point(110, 88);
            this.StopClient_Button.Name = "StopClient_Button";
            this.StopClient_Button.Size = new System.Drawing.Size(103, 29);
            this.StopClient_Button.TabIndex = 4;
            this.StopClient_Button.Text = "Stop Client";
            this.StopClient_Button.UseVisualStyleBackColor = true;
            this.StopClient_Button.Click += new System.EventHandler(this.StopClient_Button_Click);
            // 
            // Connection_GroupBox
            // 
            this.Connection_GroupBox.Controls.Add(this.Username_TextBox);
            this.Connection_GroupBox.Controls.Add(this.Username_Label);
            this.Connection_GroupBox.Controls.Add(this.LocalIP_Label);
            this.Connection_GroupBox.Controls.Add(this.IPList_ComboBox);
            this.Connection_GroupBox.Controls.Add(this.StopClient_Button);
            this.Connection_GroupBox.Controls.Add(this.StartClient_Button);
            this.Connection_GroupBox.Location = new System.Drawing.Point(12, 12);
            this.Connection_GroupBox.Name = "Connection_GroupBox";
            this.Connection_GroupBox.Size = new System.Drawing.Size(225, 123);
            this.Connection_GroupBox.TabIndex = 5;
            this.Connection_GroupBox.TabStop = false;
            this.Connection_GroupBox.Text = "Connection";
            // 
            // Username_TextBox
            // 
            this.Username_TextBox.Location = new System.Drawing.Point(81, 58);
            this.Username_TextBox.Name = "Username_TextBox";
            this.Username_TextBox.Size = new System.Drawing.Size(132, 22);
            this.Username_TextBox.TabIndex = 7;
            // 
            // Username_Label
            // 
            this.Username_Label.AutoSize = true;
            this.Username_Label.Location = new System.Drawing.Point(6, 58);
            this.Username_Label.Name = "Username_Label";
            this.Username_Label.Size = new System.Drawing.Size(77, 17);
            this.Username_Label.TabIndex = 6;
            this.Username_Label.Text = "Username:";
            // 
            // LocalIP_Label
            // 
            this.LocalIP_Label.AutoSize = true;
            this.LocalIP_Label.Location = new System.Drawing.Point(6, 27);
            this.LocalIP_Label.Name = "LocalIP_Label";
            this.LocalIP_Label.Size = new System.Drawing.Size(62, 17);
            this.LocalIP_Label.TabIndex = 5;
            this.LocalIP_Label.Text = "Local IP:";
            // 
            // ConnectedPlayers_ListBox
            // 
            this.ConnectedPlayers_ListBox.FormattingEnabled = true;
            this.ConnectedPlayers_ListBox.ItemHeight = 16;
            this.ConnectedPlayers_ListBox.Location = new System.Drawing.Point(6, 21);
            this.ConnectedPlayers_ListBox.Name = "ConnectedPlayers_ListBox";
            this.ConnectedPlayers_ListBox.Size = new System.Drawing.Size(133, 84);
            this.ConnectedPlayers_ListBox.TabIndex = 6;
            // 
            // StartGame_Button
            // 
            this.StartGame_Button.Location = new System.Drawing.Point(12, 141);
            this.StartGame_Button.Name = "StartGame_Button";
            this.StartGame_Button.Size = new System.Drawing.Size(376, 28);
            this.StartGame_Button.TabIndex = 7;
            this.StartGame_Button.Text = "Start Game";
            this.StartGame_Button.UseVisualStyleBackColor = true;
            this.StartGame_Button.Click += new System.EventHandler(this.StartGame_Button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ConnectedPlayers_ListBox);
            this.groupBox1.Location = new System.Drawing.Point(243, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(145, 123);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Players";
            // 
            // Chat_TextBox
            // 
            this.Chat_TextBox.Location = new System.Drawing.Point(-1, 581);
            this.Chat_TextBox.Name = "Chat_TextBox";
            this.Chat_TextBox.Size = new System.Drawing.Size(339, 22);
            this.Chat_TextBox.TabIndex = 9;
            // 
            // ChatSend_Button
            // 
            this.ChatSend_Button.Location = new System.Drawing.Point(339, 580);
            this.ChatSend_Button.Name = "ChatSend_Button";
            this.ChatSend_Button.Size = new System.Drawing.Size(60, 24);
            this.ChatSend_Button.TabIndex = 10;
            this.ChatSend_Button.Text = "Send";
            this.ChatSend_Button.UseVisualStyleBackColor = true;
            this.ChatSend_Button.Click += new System.EventHandler(this.ChatSend_Button_Click);
            // 
            // GMClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 603);
            this.Controls.Add(this.ChatSend_Button);
            this.Controls.Add(this.Chat_TextBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.StartGame_Button);
            this.Controls.Add(this.Connection_GroupBox);
            this.Controls.Add(this.Events_RichTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "GMClient";
            this.Text = "GM Client ";
            this.Connection_GroupBox.ResumeLayout(false);
            this.Connection_GroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox Events_RichTextBox;
        private System.Windows.Forms.ComboBox IPList_ComboBox;
        private System.Windows.Forms.Button StartClient_Button;
        private System.Windows.Forms.Button StopClient_Button;
        private System.Windows.Forms.GroupBox Connection_GroupBox;
        private System.Windows.Forms.TextBox Username_TextBox;
        private System.Windows.Forms.Label Username_Label;
        private System.Windows.Forms.Label LocalIP_Label;
        private System.Windows.Forms.ListBox ConnectedPlayers_ListBox;
        private System.Windows.Forms.Button StartGame_Button;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Chat_TextBox;
        private System.Windows.Forms.Button ChatSend_Button;
    }
}

