﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DistributedClient
{
    class PDU
    {
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct Connection_PDU
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
        public string prg;
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte version;
        [MarshalAs(UnmanagedType.U2, SizeConst = 2)]
        public short ConnectedClients;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
        public string PrimaryServer;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
        public string BackupServer;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Player_Data_PDU
    {
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte version;
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte type;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string username;
        [MarshalAs(UnmanagedType.U2, SizeConst = 2)]
        public short victories;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
        public string colour;
        [MarshalAs(UnmanagedType.U2, SizeConst = 2)]
        public short posX;
        [MarshalAs(UnmanagedType.U2, SizeConst = 2)]
        public short posY;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Player_InGame_PDU
    {
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte version;
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte type;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string username;
        [MarshalAs(UnmanagedType.U2, SizeConst = 2)]
        public short posX;
        [MarshalAs(UnmanagedType.U2, SizeConst = 2)]
        public short posY;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Player_Comm_PDU
    {
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte version;
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte type;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string username;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string msg;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Player_Serv_PDU
    {
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte version;
        [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
        public byte type;
    }
}
