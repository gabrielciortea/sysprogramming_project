﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Drawing;

namespace DistributedClient
{
    public partial class GMClient : Form
    {
        Socket _UdpClient;
        Socket _TcpConnection;
        Socket _TcpBackup;

        List<string> LocalIPs;
        List<string> EventMessages = new List<string>();
        Player[] Players;
        GameUI game;
        ClientUtilities cu = new ClientUtilities();
        MessageToServer mts = new MessageToServer();
        IPEndPoint _localIPEndPoint;
        IPEndPoint _remoteIP;
        IPEndPoint BKremoteIP;
        int NumberOfGames = 0;
        short[] StartingCoordinates = new short[4];

        bool ClientConnected = false;
        byte ServerVersion;
        string PrimaryServer;
        string BackupServer;
        private static System.Windows.Forms.Timer _ConnectionCheckTimer;
        private static System.Windows.Forms.Timer _CommunicationToServerTimer;

        public GMClient()
        {
            InitializeComponent();

            LocalIPs = cu.GetLocalIPAddress_AsString();
            foreach (String s in LocalIPs)
            {
                IPList_ComboBox.Items.Add(s);
            }
            IPList_ComboBox.SelectedIndex = 0;
            StopClient_Button.Enabled = false;
            StartGame_Button.Enabled = false;
        }

        private void InitialiseTimers()
        {
            _ConnectionCheckTimer = new System.Windows.Forms.Timer(); // Check for communication activity on Non-Blocking sockets every 200ms
            _ConnectionCheckTimer.Tick += new EventHandler(Udp_ServerCheck); // Set event handler method for timer
            _ConnectionCheckTimer.Interval = 3000;  // Timer interval is 3 second
            _ConnectionCheckTimer.Enabled = true;
            _ConnectionCheckTimer.Start();

            _CommunicationToServerTimer = new System.Windows.Forms.Timer(); // Check for communication activity on Non-Blocking sockets every 200ms
            _CommunicationToServerTimer.Tick += new EventHandler(Tcp_ServerCommunication); // Set event handler method for timer
            _CommunicationToServerTimer.Interval = 50;  // Timer interval is 0.1 second
            _CommunicationToServerTimer.Enabled = false;
        }

        private void Tcp_ServerCommunication(object sender, EventArgs e)
        {
            try
            {
                EndPoint localEndPoint = (EndPoint)_localIPEndPoint;
                byte[] ReceiveBuffer = new byte[1024];
                int ReceivedByteCount;
                ReceivedByteCount = _TcpConnection.ReceiveFrom(ReceiveBuffer, ref localEndPoint);
                byte[] PlayerPDU = new byte[ReceivedByteCount];
                Array.Copy(ReceiveBuffer, PlayerPDU, ReceivedByteCount);

                if (0 < ReceivedByteCount)
                {
                    

                    if (PlayerPDU[0] == 0b00000001) // then it is version 1
                    {
                        if (PlayerPDU[1] == 0b00000111) // Receives all data of  a player from server
                        {
                            Player_Data_PDU P_PDU = cu.DeSerializePlayerDataPDU(PlayerPDU);
                            if (P_PDU.username == Players[0].Username)
                            {
                                Players[0].Victories = P_PDU.victories;
                                Players[0].Colour = P_PDU.colour;
                                Players[0].PosX = P_PDU.posX;
                                Players[0].PosY = P_PDU.posY;
                            }
                            if (P_PDU.username != Players[0].Username)
                            {
                                Players[1] = new Player(P_PDU.username);
                                Players[1].Victories = P_PDU.victories;
                                Players[1].Colour = P_PDU.colour;
                                Players[1].PosX = P_PDU.posX;
                                Players[1].PosY = P_PDU.posY;
                                ConnectedPlayers_ListBox.Items.Add(Players[1].Username);
                            }
                        }
                        if (PlayerPDU[1] == 0b10000011) // Starting coordinates
                        {
                            Player_Data_PDU P_PDU = cu.DeSerializePlayerDataPDU(PlayerPDU);
                            StartingCoordinates[0] = P_PDU.posX;
                            StartingCoordinates[1] = P_PDU.posY;
                        }
                        if (PlayerPDU[1] == 0b00000011) // Chat message
                        {
                            Player_Comm_PDU P_PDU = cu.DeSerializePlayerCommPDU(PlayerPDU);
                            EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + P_PDU.msg);
                            cu.PrintEventLog(EventMessages, Events_RichTextBox);
                        }
                        if (PlayerPDU[1] == 0b01010101) // Player disconnected
                        {
                            Player_Comm_PDU P_PDU = cu.DeSerializePlayerCommPDU(PlayerPDU);
                            EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + P_PDU.msg);
                            ConnectedPlayers_ListBox.Items.Remove((Object)P_PDU.username);
                            cu.PrintEventLog(EventMessages, Events_RichTextBox);
                        }
                        if (PlayerPDU[1] == 0b00010001) // Two players in the room
                        {
                            StartGame_Button.Enabled = true;
                        }
                        if (PlayerPDU[1] == 0b11111111) // Winning signal received
                        {
                            Player_Comm_PDU P_PDU = cu.DeSerializePlayerCommPDU(PlayerPDU);
                            game.ReceivedWinningSignal(P_PDU.username);
                            game = null;
                            StartGame_Button.Enabled = true;
                            Players[0].PosX = StartingCoordinates[0];
                            Players[0].PosY = StartingCoordinates[1];
                        }
                        if (PlayerPDU[1] == 0b11001111) // If primary server failed, bk server picked a new socked and awaits username to assigned in bk structure
                        {
                            mts.Tcp_SendPlayerUpdatePDUForBackupServer(Players[0], _TcpConnection);
                        }
                        if (PlayerPDU[1] == 0b11111001) // Ball fetched
                        {
                            Player_Comm_PDU P_PDU = cu.DeSerializePlayerCommPDU(PlayerPDU);
                            game.DisableBall(P_PDU.msg);
                        }
                        if (PlayerPDU[1] == 0b11111000) // Fence disabled
                        {
                            Player_Comm_PDU P_PDU = cu.DeSerializePlayerCommPDU(PlayerPDU);
                            game.DisableFence(P_PDU.msg);
                        }
                        if (PlayerPDU[1] == 0b10100010) // Victories count
                        {
                            Player_Comm_PDU P_PDU = cu.DeSerializePlayerCommPDU(PlayerPDU);
                            EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + P_PDU.msg);
                            cu.PrintEventLog(EventMessages, Events_RichTextBox);
                        }
                        if (PlayerPDU[1] == 0b10001000) // Player started the game
                        {
                            if (NumberOfGames == 0)
                            {
                                StartingCoordinates[2] = Players[1].PosX;
                                StartingCoordinates[3] = Players[1].PosY;
                            }
                            else
                            {
                                Players[1].PosX = StartingCoordinates[2];
                                Players[1].PosY = StartingCoordinates[3];
                            }
                            NumberOfGames++;
                            EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Game starting...");
                            System.Threading.Thread.Sleep(1000);
                            cu.PrintEventLog(EventMessages, Events_RichTextBox);
                            StartGame_Button.Enabled = false;
                            game = new GameUI(ref Players, ref _TcpConnection);
                            game.Show();
                        }
                        if (PlayerPDU[1] == 0b11001100) // opponent position update
                        {
                            Player_InGame_PDU PG_PDU = cu.DeSerializePlayerInGamePDU(PlayerPDU);
                            if (PG_PDU.username == Players[1].Username)
                            {
                                Players[1].PosX = PG_PDU.posX;
                                Players[1].PosY = PG_PDU.posY;
                                game.UpdateOpponentPlayerPosition();
                            }
                        }
                    }

                }
            }
            catch (SocketException se) // Handle socket-related exception
            {   // If an exception occurs, display an error message
                if (10053 == se.ErrorCode || 10054 == se.ErrorCode) // Remote end closed the connection
                {
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Server Stopped.. Trying to connect to backup server");
                    _TcpBackup = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _TcpBackup.Blocking = true;
                    System.Net.IPAddress BKServerIp = System.Net.IPAddress.Parse(BackupServer);
                    BKremoteIP = new System.Net.IPEndPoint(BKServerIp, 8001);
                    System.Threading.Thread.Sleep(200);
                    bool MsgShown = false;
                    try
                    {
                        _TcpBackup.Connect(BKremoteIP);
                        _TcpBackup.Blocking = false;
                        _TcpConnection = _TcpBackup;
                        if (game != null)
                        {
                            game.UpdateInGameSocket(_TcpConnection);
                        }
                        cu.PrintEventLog(EventMessages, Events_RichTextBox);
                    }
                    catch (SocketException SE)
                    {
                        if (MsgShown == false)
                        {
                            MessageBox.Show("BK server not found");
                            MsgShown = true;
                        }
                        System.Windows.Forms.Application.Exit();
                    }
                }
                else if (10035 != se.ErrorCode)
                {   // Ignore error messages relating to normal behaviour of non-blocking sockets
                    MessageBox.Show(se.Message);
                }
            }
            catch // Silently handle any other exception
            {
            }
        }

        private void Udp_ServerCheck(object sender, EventArgs e)
        {
            try
            {
                EndPoint localEndPoint = (EndPoint)_localIPEndPoint;
                byte[] ReceiveBuffer = new byte[1024];
                int iReceiveByteCount;
                iReceiveByteCount = _UdpClient.ReceiveFrom(ReceiveBuffer, ref localEndPoint);
                byte[] ConnectionPDU = new byte[iReceiveByteCount];
                Array.Copy(ReceiveBuffer, ConnectionPDU, iReceiveByteCount);

                if (0 < iReceiveByteCount)
                {   // Copy the number of bytes received, from the message buffer to the text control
                    Connection_PDU ConPDU = cu.DeSerialize(ConnectionPDU);
                    ServerVersion = ConPDU.version;
                    PrimaryServer = ConPDU.PrimaryServer;
                    BackupServer = ConPDU.BackupServer;
                    if ((ConPDU.prg == "PSGM") && (ServerVersion == 0b00000001) && (ClientConnected == false))
                    {
                        System.Net.IPAddress serverIP = System.Net.IPAddress.Parse(ConPDU.PrimaryServer);
                        _remoteIP = new System.Net.IPEndPoint(serverIP, 8001);
                        _TcpConnection.Connect(_remoteIP);
                        _TcpConnection.Blocking = false;
                        EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": *** Connected to Game Server ***");
                        cu.PrintEventLog(EventMessages, Events_RichTextBox);
                        ClientConnected = true;
                        Players[0] = new Player(Username_TextBox.Text);
                        ConnectedPlayers_ListBox.Items.Add(Players[0].Username);
                        mts.Tcp_SendPlayerUpdatePDU(Players[0], _TcpConnection);
                        _CommunicationToServerTimer.Enabled = true;
                        _CommunicationToServerTimer.Start();
                    }
                }
            }
            catch (SocketException se) // Catch any errors
            {   // Display a diagnostic message
                EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": *** Server not found ***");
                cu.PrintEventLog(EventMessages, Events_RichTextBox);
            }
        }

        private void StartClient_Button_Click(object sender, EventArgs e)
        {
            if (Username_TextBox.Text.Length <= 10)
            {
                try
                {
                    Players = new Player[2];
                    _UdpClient = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    _TcpConnection = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _UdpClient.Blocking = false;
                    _TcpConnection.Blocking = true;
                    _localIPEndPoint = new System.Net.IPEndPoint(IPAddress.Any, 8000);
                    _UdpClient.Bind(_localIPEndPoint);
                    EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + ": Binded and checking for servers");
                    cu.PrintEventLog(EventMessages, Events_RichTextBox);
                    Username_TextBox.Enabled = false;
                    StartClient_Button.Enabled = false;
                    StopClient_Button.Enabled = true;
                }
                catch (SocketException se)
                {
                    MessageBox.Show(se.Message);
                }
                InitialiseTimers();
            }
            else
            {
                MessageBox.Show("Username must be maximun 10 characters");
            }
            
        }

        private void StartGame_Button_Click(object sender, EventArgs e)
        {
            cu.PrintEventLog(EventMessages, Events_RichTextBox);
            System.Threading.Thread.Sleep(100);
            mts.Tcp_SendStartGameSignal(Players[0], "StartGame!", _TcpConnection);
        }

        private void ChatSend_Button_Click(object sender, EventArgs e)
        {
            string ChatMessage = Chat_TextBox.Text;
            mts.Tcp_SendChatMessage(Players[0], ChatMessage, _TcpConnection);
            Chat_TextBox.Clear();
        }

        private void StopClient_Button_Click(object sender, EventArgs e)
        {
            mts.Tcp_SendDisconnectMessage(Players[0], _TcpConnection);
            cu.StopClient(_TcpConnection, _UdpClient);
            Players = null;
            ClientConnected = false;
            _ConnectionCheckTimer.Stop();
            _CommunicationToServerTimer.Stop();
            StartClient_Button.Enabled = true;
            StopClient_Button.Enabled = false;
            EventMessages.Add(DateTime.Now.ToString("hh:mm:ss tt") + " Client Stopped");
            ConnectedPlayers_ListBox.Items.Clear();
            Username_TextBox.Clear();
            Username_TextBox.Enabled = true;
            cu.PrintEventLog(EventMessages, Events_RichTextBox);
        }
    }
}
