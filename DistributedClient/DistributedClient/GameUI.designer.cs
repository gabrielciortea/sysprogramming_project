﻿namespace DistributedClient
{
    partial class GameUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameUI));
            this.LocalTimer = new System.Windows.Forms.Timer(this.components);
            this.Maze_Panel = new System.Windows.Forms.Panel();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.Fence_PictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.DogHouse2_PictureBox = new System.Windows.Forms.PictureBox();
            this.DogHouse1_PictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.Winner_PictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Maze_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fence_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DogHouse2_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DogHouse1_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Winner_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // LocalTimer
            // 
            this.LocalTimer.Enabled = true;
            this.LocalTimer.Interval = 20;
            this.LocalTimer.Tick += new System.EventHandler(this.LocalTimer_Tick);
            // 
            // Maze_Panel
            // 
            this.Maze_Panel.BackColor = System.Drawing.SystemColors.Control;
            this.Maze_Panel.Controls.Add(this.pictureBox41);
            this.Maze_Panel.Controls.Add(this.pictureBox53);
            this.Maze_Panel.Controls.Add(this.pictureBox36);
            this.Maze_Panel.Controls.Add(this.pictureBox60);
            this.Maze_Panel.Controls.Add(this.pictureBox59);
            this.Maze_Panel.Controls.Add(this.pictureBox52);
            this.Maze_Panel.Controls.Add(this.pictureBox51);
            this.Maze_Panel.Controls.Add(this.pictureBox50);
            this.Maze_Panel.Controls.Add(this.pictureBox49);
            this.Maze_Panel.Controls.Add(this.Fence_PictureBox);
            this.Maze_Panel.Controls.Add(this.pictureBox48);
            this.Maze_Panel.Controls.Add(this.pictureBox47);
            this.Maze_Panel.Controls.Add(this.pictureBox46);
            this.Maze_Panel.Controls.Add(this.pictureBox45);
            this.Maze_Panel.Controls.Add(this.pictureBox44);
            this.Maze_Panel.Controls.Add(this.pictureBox43);
            this.Maze_Panel.Controls.Add(this.pictureBox42);
            this.Maze_Panel.Controls.Add(this.pictureBox40);
            this.Maze_Panel.Controls.Add(this.pictureBox39);
            this.Maze_Panel.Controls.Add(this.pictureBox38);
            this.Maze_Panel.Controls.Add(this.pictureBox37);
            this.Maze_Panel.Controls.Add(this.pictureBox35);
            this.Maze_Panel.Controls.Add(this.pictureBox34);
            this.Maze_Panel.Controls.Add(this.pictureBox32);
            this.Maze_Panel.Controls.Add(this.pictureBox33);
            this.Maze_Panel.Controls.Add(this.pictureBox31);
            this.Maze_Panel.Controls.Add(this.pictureBox30);
            this.Maze_Panel.Controls.Add(this.pictureBox29);
            this.Maze_Panel.Controls.Add(this.pictureBox28);
            this.Maze_Panel.Controls.Add(this.pictureBox26);
            this.Maze_Panel.Controls.Add(this.pictureBox25);
            this.Maze_Panel.Controls.Add(this.pictureBox27);
            this.Maze_Panel.Controls.Add(this.DogHouse2_PictureBox);
            this.Maze_Panel.Controls.Add(this.DogHouse1_PictureBox);
            this.Maze_Panel.Controls.Add(this.pictureBox24);
            this.Maze_Panel.Controls.Add(this.pictureBox18);
            this.Maze_Panel.Controls.Add(this.pictureBox23);
            this.Maze_Panel.Controls.Add(this.pictureBox22);
            this.Maze_Panel.Controls.Add(this.pictureBox21);
            this.Maze_Panel.Controls.Add(this.pictureBox20);
            this.Maze_Panel.Controls.Add(this.pictureBox19);
            this.Maze_Panel.Controls.Add(this.pictureBox17);
            this.Maze_Panel.Controls.Add(this.pictureBox16);
            this.Maze_Panel.Controls.Add(this.pictureBox15);
            this.Maze_Panel.Controls.Add(this.pictureBox14);
            this.Maze_Panel.Controls.Add(this.pictureBox13);
            this.Maze_Panel.Controls.Add(this.pictureBox12);
            this.Maze_Panel.Controls.Add(this.pictureBox11);
            this.Maze_Panel.Controls.Add(this.pictureBox10);
            this.Maze_Panel.Controls.Add(this.pictureBox9);
            this.Maze_Panel.Controls.Add(this.pictureBox8);
            this.Maze_Panel.Controls.Add(this.pictureBox4);
            this.Maze_Panel.Controls.Add(this.pictureBox5);
            this.Maze_Panel.Controls.Add(this.pictureBox1);
            this.Maze_Panel.Controls.Add(this.pictureBox7);
            this.Maze_Panel.Controls.Add(this.pictureBox6);
            this.Maze_Panel.Controls.Add(this.pictureBox3);
            this.Maze_Panel.Controls.Add(this.Winner_PictureBox);
            this.Maze_Panel.Controls.Add(this.pictureBox2);
            this.Maze_Panel.Location = new System.Drawing.Point(0, -1);
            this.Maze_Panel.Name = "Maze_Panel";
            this.Maze_Panel.Size = new System.Drawing.Size(1084, 654);
            this.Maze_Panel.TabIndex = 16;
            this.Maze_Panel.Tag = "hole";
            // 
            // pictureBox41
            // 
            this.pictureBox41.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox41.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox41.Location = new System.Drawing.Point(1073, 3);
            this.pictureBox41.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(10, 648);
            this.pictureBox41.TabIndex = 83;
            this.pictureBox41.TabStop = false;
            this.pictureBox41.Tag = "platform";
            // 
            // pictureBox53
            // 
            this.pictureBox53.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox53.Image")));
            this.pictureBox53.Location = new System.Drawing.Point(914, 8);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(24, 24);
            this.pictureBox53.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox53.TabIndex = 82;
            this.pictureBox53.TabStop = false;
            this.pictureBox53.Tag = "ball03";
            // 
            // pictureBox36
            // 
            this.pictureBox36.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox36.Image")));
            this.pictureBox36.Location = new System.Drawing.Point(915, 618);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(24, 24);
            this.pictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox36.TabIndex = 81;
            this.pictureBox36.TabStop = false;
            this.pictureBox36.Tag = "ball04";
            // 
            // pictureBox60
            // 
            this.pictureBox60.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox60.Image")));
            this.pictureBox60.Location = new System.Drawing.Point(510, 339);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(24, 24);
            this.pictureBox60.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox60.TabIndex = 80;
            this.pictureBox60.TabStop = false;
            this.pictureBox60.Tag = "ball01";
            // 
            // pictureBox59
            // 
            this.pictureBox59.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox59.Image")));
            this.pictureBox59.Location = new System.Drawing.Point(510, 291);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(24, 24);
            this.pictureBox59.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox59.TabIndex = 79;
            this.pictureBox59.TabStop = false;
            this.pictureBox59.Tag = "ball02";
            // 
            // pictureBox52
            // 
            this.pictureBox52.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox52.Image")));
            this.pictureBox52.Location = new System.Drawing.Point(17, 274);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(32, 32);
            this.pictureBox52.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox52.TabIndex = 72;
            this.pictureBox52.TabStop = false;
            // 
            // pictureBox51
            // 
            this.pictureBox51.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox51.Image")));
            this.pictureBox51.Location = new System.Drawing.Point(300, 274);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(32, 32);
            this.pictureBox51.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox51.TabIndex = 71;
            this.pictureBox51.TabStop = false;
            // 
            // pictureBox50
            // 
            this.pictureBox50.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox50.Image")));
            this.pictureBox50.Location = new System.Drawing.Point(301, 342);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(32, 32);
            this.pictureBox50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox50.TabIndex = 70;
            this.pictureBox50.TabStop = false;
            // 
            // pictureBox49
            // 
            this.pictureBox49.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox49.Image")));
            this.pictureBox49.Location = new System.Drawing.Point(17, 341);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(32, 32);
            this.pictureBox49.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox49.TabIndex = 69;
            this.pictureBox49.TabStop = false;
            // 
            // Fence_PictureBox
            // 
            this.Fence_PictureBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Fence_PictureBox.BackColor = System.Drawing.Color.Red;
            this.Fence_PictureBox.Location = new System.Drawing.Point(665, 252);
            this.Fence_PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Fence_PictureBox.Name = "Fence_PictureBox";
            this.Fence_PictureBox.Size = new System.Drawing.Size(10, 147);
            this.Fence_PictureBox.TabIndex = 68;
            this.Fence_PictureBox.TabStop = false;
            this.Fence_PictureBox.Tag = "platform";
            // 
            // pictureBox48
            // 
            this.pictureBox48.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox48.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox48.Location = new System.Drawing.Point(665, 322);
            this.pictureBox48.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(414, 10);
            this.pictureBox48.TabIndex = 67;
            this.pictureBox48.TabStop = false;
            this.pictureBox48.Tag = "platform";
            // 
            // pictureBox47
            // 
            this.pictureBox47.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox47.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox47.Location = new System.Drawing.Point(997, 397);
            this.pictureBox47.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(10, 105);
            this.pictureBox47.TabIndex = 66;
            this.pictureBox47.TabStop = false;
            this.pictureBox47.Tag = "platform";
            // 
            // pictureBox46
            // 
            this.pictureBox46.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox46.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox46.Location = new System.Drawing.Point(997, 149);
            this.pictureBox46.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(10, 105);
            this.pictureBox46.TabIndex = 65;
            this.pictureBox46.TabStop = false;
            this.pictureBox46.Tag = "platform";
            // 
            // pictureBox45
            // 
            this.pictureBox45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox45.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox45.Location = new System.Drawing.Point(558, 244);
            this.pictureBox45.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(448, 10);
            this.pictureBox45.TabIndex = 64;
            this.pictureBox45.TabStop = false;
            this.pictureBox45.Tag = "platform";
            // 
            // pictureBox44
            // 
            this.pictureBox44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox44.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox44.Location = new System.Drawing.Point(558, 397);
            this.pictureBox44.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(442, 10);
            this.pictureBox44.TabIndex = 63;
            this.pictureBox44.TabStop = false;
            this.pictureBox44.Tag = "platform";
            // 
            // pictureBox43
            // 
            this.pictureBox43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox43.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox43.Location = new System.Drawing.Point(609, 192);
            this.pictureBox43.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(348, 10);
            this.pictureBox43.TabIndex = 62;
            this.pictureBox43.TabStop = false;
            this.pictureBox43.Tag = "platform";
            // 
            // pictureBox42
            // 
            this.pictureBox42.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox42.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox42.Location = new System.Drawing.Point(948, 3);
            this.pictureBox42.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(11, 199);
            this.pictureBox42.TabIndex = 61;
            this.pictureBox42.TabStop = false;
            this.pictureBox42.Tag = "platform";
            // 
            // pictureBox40
            // 
            this.pictureBox40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox40.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox40.Location = new System.Drawing.Point(611, 448);
            this.pictureBox40.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(348, 10);
            this.pictureBox40.TabIndex = 59;
            this.pictureBox40.TabStop = false;
            this.pictureBox40.Tag = "platform";
            // 
            // pictureBox39
            // 
            this.pictureBox39.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox39.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox39.Location = new System.Drawing.Point(949, 451);
            this.pictureBox39.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(11, 199);
            this.pictureBox39.TabIndex = 58;
            this.pictureBox39.TabStop = false;
            this.pictureBox39.Tag = "platform";
            // 
            // pictureBox38
            // 
            this.pictureBox38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox38.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox38.Location = new System.Drawing.Point(558, 496);
            this.pictureBox38.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(348, 10);
            this.pictureBox38.TabIndex = 57;
            this.pictureBox38.TabStop = false;
            this.pictureBox38.Tag = "platform";
            // 
            // pictureBox37
            // 
            this.pictureBox37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox37.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox37.Location = new System.Drawing.Point(558, 145);
            this.pictureBox37.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(348, 10);
            this.pictureBox37.TabIndex = 56;
            this.pictureBox37.TabStop = false;
            this.pictureBox37.Tag = "platform";
            // 
            // pictureBox35
            // 
            this.pictureBox35.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox35.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox35.Location = new System.Drawing.Point(549, 145);
            this.pictureBox35.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(10, 361);
            this.pictureBox35.TabIndex = 54;
            this.pictureBox35.TabStop = false;
            this.pictureBox35.Tag = "platform";
            // 
            // pictureBox34
            // 
            this.pictureBox34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox34.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox34.Location = new System.Drawing.Point(494, 90);
            this.pictureBox34.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(10, 472);
            this.pictureBox34.TabIndex = 53;
            this.pictureBox34.TabStop = false;
            this.pictureBox34.Tag = "platform";
            // 
            // pictureBox32
            // 
            this.pictureBox32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox32.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox32.Location = new System.Drawing.Point(548, 552);
            this.pictureBox32.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(348, 10);
            this.pictureBox32.TabIndex = 50;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.Tag = "platform";
            // 
            // pictureBox33
            // 
            this.pictureBox33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox33.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox33.Location = new System.Drawing.Point(896, 552);
            this.pictureBox33.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(12, 98);
            this.pictureBox33.TabIndex = 49;
            this.pictureBox33.TabStop = false;
            this.pictureBox33.Tag = "platform";
            // 
            // pictureBox31
            // 
            this.pictureBox31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox31.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox31.Location = new System.Drawing.Point(548, 92);
            this.pictureBox31.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(348, 10);
            this.pictureBox31.TabIndex = 48;
            this.pictureBox31.TabStop = false;
            this.pictureBox31.Tag = "platform";
            // 
            // pictureBox30
            // 
            this.pictureBox30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox30.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox30.Location = new System.Drawing.Point(896, 4);
            this.pictureBox30.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(12, 98);
            this.pictureBox30.TabIndex = 47;
            this.pictureBox30.TabStop = false;
            this.pictureBox30.Tag = "platform";
            // 
            // pictureBox29
            // 
            this.pictureBox29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox29.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox29.Location = new System.Drawing.Point(449, 598);
            this.pictureBox29.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(405, 10);
            this.pictureBox29.TabIndex = 46;
            this.pictureBox29.TabStop = false;
            this.pictureBox29.Tag = "platform";
            // 
            // pictureBox28
            // 
            this.pictureBox28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox28.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox28.Location = new System.Drawing.Point(447, 45);
            this.pictureBox28.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(407, 10);
            this.pictureBox28.TabIndex = 45;
            this.pictureBox28.TabStop = false;
            this.pictureBox28.Tag = "platform";
            // 
            // pictureBox26
            // 
            this.pictureBox26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox26.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox26.Location = new System.Drawing.Point(448, 408);
            this.pictureBox26.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(11, 200);
            this.pictureBox26.TabIndex = 44;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.Tag = "platform";
            // 
            // pictureBox25
            // 
            this.pictureBox25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox25.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox25.Location = new System.Drawing.Point(448, 45);
            this.pictureBox25.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(11, 221);
            this.pictureBox25.TabIndex = 43;
            this.pictureBox25.TabStop = false;
            this.pictureBox25.Tag = "platform";
            // 
            // pictureBox27
            // 
            this.pictureBox27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox27.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox27.Location = new System.Drawing.Point(3, 322);
            this.pictureBox27.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(548, 10);
            this.pictureBox27.TabIndex = 41;
            this.pictureBox27.TabStop = false;
            this.pictureBox27.Tag = "platform";
            // 
            // DogHouse2_PictureBox
            // 
            this.DogHouse2_PictureBox.Image = ((System.Drawing.Image)(resources.GetObject("DogHouse2_PictureBox.Image")));
            this.DogHouse2_PictureBox.Location = new System.Drawing.Point(261, 232);
            this.DogHouse2_PictureBox.Name = "DogHouse2_PictureBox";
            this.DogHouse2_PictureBox.Size = new System.Drawing.Size(24, 24);
            this.DogHouse2_PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.DogHouse2_PictureBox.TabIndex = 39;
            this.DogHouse2_PictureBox.TabStop = false;
            this.DogHouse2_PictureBox.Tag = "kennel";
            // 
            // DogHouse1_PictureBox
            // 
            this.DogHouse1_PictureBox.Image = ((System.Drawing.Image)(resources.GetObject("DogHouse1_PictureBox.Image")));
            this.DogHouse1_PictureBox.Location = new System.Drawing.Point(261, 395);
            this.DogHouse1_PictureBox.Name = "DogHouse1_PictureBox";
            this.DogHouse1_PictureBox.Size = new System.Drawing.Size(24, 24);
            this.DogHouse1_PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.DogHouse1_PictureBox.TabIndex = 38;
            this.DogHouse1_PictureBox.TabStop = false;
            this.DogHouse1_PictureBox.Tag = "kennel";
            // 
            // pictureBox24
            // 
            this.pictureBox24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox24.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox24.Location = new System.Drawing.Point(60, 216);
            this.pictureBox24.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(248, 11);
            this.pictureBox24.TabIndex = 37;
            this.pictureBox24.TabStop = false;
            this.pictureBox24.Tag = "platform";
            // 
            // pictureBox18
            // 
            this.pictureBox18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox18.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox18.Location = new System.Drawing.Point(61, 127);
            this.pictureBox18.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(248, 11);
            this.pictureBox18.TabIndex = 36;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Tag = "platform";
            // 
            // pictureBox23
            // 
            this.pictureBox23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox23.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox23.Location = new System.Drawing.Point(60, 425);
            this.pictureBox23.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(248, 11);
            this.pictureBox23.TabIndex = 35;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.Tag = "platform";
            // 
            // pictureBox22
            // 
            this.pictureBox22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox22.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox22.Location = new System.Drawing.Point(8, 174);
            this.pictureBox22.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(251, 10);
            this.pictureBox22.TabIndex = 32;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.Tag = "platform";
            // 
            // pictureBox21
            // 
            this.pictureBox21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox21.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox21.Location = new System.Drawing.Point(8, 470);
            this.pictureBox21.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(245, 10);
            this.pictureBox21.TabIndex = 31;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Tag = "platform";
            // 
            // pictureBox20
            // 
            this.pictureBox20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox20.Location = new System.Drawing.Point(0, 4);
            this.pictureBox20.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(11, 328);
            this.pictureBox20.TabIndex = 30;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Tag = "platform";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox19.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox19.Location = new System.Drawing.Point(0, 329);
            this.pictureBox19.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(11, 321);
            this.pictureBox19.TabIndex = 29;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Tag = "platform";
            // 
            // pictureBox17
            // 
            this.pictureBox17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox17.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox17.Location = new System.Drawing.Point(61, 515);
            this.pictureBox17.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(248, 11);
            this.pictureBox17.TabIndex = 27;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Tag = "platform";
            // 
            // pictureBox16
            // 
            this.pictureBox16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox16.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox16.Location = new System.Drawing.Point(299, 137);
            this.pictureBox16.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(10, 132);
            this.pictureBox16.TabIndex = 26;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Tag = "platform";
            // 
            // pictureBox15
            // 
            this.pictureBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox15.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox15.Location = new System.Drawing.Point(-1, 261);
            this.pictureBox15.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(409, 10);
            this.pictureBox15.TabIndex = 25;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Tag = "platform";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox14.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox14.Location = new System.Drawing.Point(-1, 383);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(409, 10);
            this.pictureBox14.TabIndex = 24;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Tag = "platform";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox13.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox13.Location = new System.Drawing.Point(399, 4);
            this.pictureBox13.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(10, 266);
            this.pictureBox13.TabIndex = 23;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Tag = "platform";
            // 
            // pictureBox12
            // 
            this.pictureBox12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox12.Location = new System.Drawing.Point(347, 37);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(10, 147);
            this.pictureBox12.TabIndex = 22;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Tag = "platform";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox11.Location = new System.Drawing.Point(275, 603);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(82, 10);
            this.pictureBox11.TabIndex = 21;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Tag = "platform";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox10.Location = new System.Drawing.Point(275, 37);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(10, 50);
            this.pictureBox10.TabIndex = 20;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Tag = "platform";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox9.Location = new System.Drawing.Point(275, 563);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(10, 50);
            this.pictureBox9.TabIndex = 19;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Tag = "platform";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox8.Location = new System.Drawing.Point(0, 563);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(274, 11);
            this.pictureBox8.TabIndex = 18;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Tag = "platform";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.GrayText;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(0, 649);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1084, 5);
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "platform";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.GrayText;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(1084, 5);
            this.pictureBox5.TabIndex = 16;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Tag = "platform";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox1.Location = new System.Drawing.Point(0, 77);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(280, 10);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "platform";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox7.Location = new System.Drawing.Point(347, 466);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 147);
            this.pictureBox7.TabIndex = 15;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Tag = "platform";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox6.Location = new System.Drawing.Point(275, 37);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(82, 10);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Tag = "platform";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox3.Location = new System.Drawing.Point(399, 384);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 266);
            this.pictureBox3.TabIndex = 14;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "platform";
            // 
            // Winner_PictureBox
            // 
            this.Winner_PictureBox.Image = global::DistributedClient.Properties.Resources.dog_bone__1_;
            this.Winner_PictureBox.Location = new System.Drawing.Point(575, 298);
            this.Winner_PictureBox.Name = "Winner_PictureBox";
            this.Winner_PictureBox.Size = new System.Drawing.Size(60, 60);
            this.Winner_PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Winner_PictureBox.TabIndex = 12;
            this.Winner_PictureBox.TabStop = false;
            this.Winner_PictureBox.Tag = "win";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox2.Location = new System.Drawing.Point(299, 384);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 132);
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "platform";
            // 
            // GameUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1082, 653);
            this.Controls.Add(this.Maze_Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1100, 700);
            this.Name = "GameUI";
            this.Text = "GameUI";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyisdown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyisup);
            this.Maze_Panel.ResumeLayout(false);
            this.Maze_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fence_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DogHouse2_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DogHouse1_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Winner_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer LocalTimer;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox Winner_PictureBox;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel Maze_Panel;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox DogHouse2_PictureBox;
        private System.Windows.Forms.PictureBox DogHouse1_PictureBox;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox Fence_PictureBox;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.PictureBox pictureBox60;
        private System.Windows.Forms.PictureBox pictureBox59;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox41;
    }
}