﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributedClient
{
    class ClientUtilities
    {
        public void PrintEventLog(List<string> l, RichTextBox rtb)
        {
            string allEvents = "";
            foreach (string s in l)
            {
                allEvents = allEvents + s + "\n";
            }
            rtb.Text = allEvents;
        }

        public List<string> GetLocalIPAddress_AsString()
        {
            List<string> allIPs = new List<string>();
            allIPs.Add("127.0.0.1");  // Default is local loopback address
            IPHostEntry IPHost = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress IP in IPHost.AddressList)
            {
                if (IP.AddressFamily == AddressFamily.InterNetwork) // Match only the IPv4 address
                {
                    allIPs.Add(IP.ToString());
                    break;
                }
            }
            return allIPs;
        }

        public void StopClient(Socket _t, Socket _u)
        {
            try
            {
                _t.Shutdown(SocketShutdown.Both);
                _t.Close();
                _u.Shutdown(SocketShutdown.Both);
                _u.Close();
            }
            catch // Silently handle any exceptions
            {
            }
        }

        //converts any object into a byte[]
        public byte[] Serialize(Object myObject)
        {
            int size = Marshal.SizeOf(myObject);
            IntPtr ip = Marshal.AllocHGlobal(size); //allocate unmanaged memory equivelent to the size of the object
            Marshal.StructureToPtr(myObject, ip, false); //marshal the object into the allocated memory
            byte[] byteArray = new byte[size];
            Marshal.Copy(ip, byteArray, 0, size); //place the contents of memory into a byte[]
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return byteArray;
        }

        //converts a byte[] to a Message_PDU structure
        public Connection_PDU DeSerialize(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Connection_PDU structure = (Connection_PDU)Marshal.PtrToStructure(ip, typeof(Connection_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        public Player_Data_PDU DeSerializePlayerDataPDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Player_Data_PDU structure = (Player_Data_PDU)Marshal.PtrToStructure(ip, typeof(Player_Data_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        public Player_InGame_PDU DeSerializePlayerInGamePDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Player_InGame_PDU structure = (Player_InGame_PDU)Marshal.PtrToStructure(ip, typeof(Player_InGame_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        public Player_Comm_PDU DeSerializePlayerCommPDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Player_Comm_PDU structure = (Player_Comm_PDU)Marshal.PtrToStructure(ip, typeof(Player_Comm_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }

        public Player_Serv_PDU DeSerializePlayerServPDU(byte[] received)
        {
            IntPtr ip = Marshal.AllocHGlobal(received.Length); //allocate unmanaged memory
            Marshal.Copy(received, 0, ip, received.Length); //copy the byte[] to the unmanaged memory
            Player_Serv_PDU structure = (Player_Serv_PDU)Marshal.PtrToStructure(ip, typeof(Player_Serv_PDU)); //Marshal the unmanaged memory contents to the Message_PDU structure
            Marshal.FreeHGlobal(ip); //free unmanaged memory
            return structure;
        }
    }
}
