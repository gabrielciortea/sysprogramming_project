﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DistributedClient
{
    public partial class GameUI : Form
    {
        PictureBox[] playerBox = new PictureBox[4];
        Socket _TcpClientInGame;
        Player[] PlayersInGame;
        MessageToServer mts = new MessageToServer();
        bool GameWon = false;
        bool goLeft = false;
        bool goRight = false;
        bool goUp = false;
        bool goDown = false;

        public GameUI(ref Player[] Players, ref Socket _TcpClient)
        {
            InitializeComponent();
            //string url = "https://i.postimg.cc/Y0NRKyz2/dog1.png";
            for (int i = 0; i < 2; i++)
            {
                playerBox[i] = new PictureBox();
                playerBox[i].Location = new Point(Players[i].PosX, Players[i].PosY);
                playerBox[i].Name = Players[i].Username;
                playerBox[i].BackColor = Color.FromName(Players[i].Colour);
                playerBox[i].Image = Properties.Resources.dog;
                playerBox[i].Size = new Size(25, 25);
                playerBox[i].Tag = "mys";
                Maze_Panel.Controls.Add(playerBox[i]);
            }
            _TcpClientInGame = _TcpClient;
            PlayersInGame = Players;

        }

        private void keyisdown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goLeft = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                goRight = true;
            }
            if(e.KeyCode == Keys.Up)
            {
                goUp = true;
            }
            if(e.KeyCode == Keys.Down)
            {
                goDown = true;
            }

        }

        private void keyisup(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goLeft = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                goRight = false;
            }
            if (e.KeyCode == Keys.Up)
            {
                goUp = false;
            }
            if (e.KeyCode == Keys.Down)
            {
                goDown = false;
            }
        }

        private void LocalTimer_Tick(object sender, EventArgs e)
        {
            if (goLeft)
            {
                playerBox[0].Left -= 5;
                UpdateLocalPlayerPosition();
            }
            if (goRight)
            {
                playerBox[0].Left += 5;
                UpdateLocalPlayerPosition();
            }
            if (goUp)
            {
                playerBox[0].Top -= 5;
                UpdateLocalPlayerPosition();
            }
            if (goDown)
            {
                playerBox[0].Top += 5;
                UpdateLocalPlayerPosition();
            }
            
            foreach (Control x in Maze_Panel.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "kennel")
                {
                    if (playerBox[0].Bounds.IntersectsWith(x.Bounds))
                    {
                        Point CurrentLocation = GetRandomPositionFromKennel();
                        playerBox[0].Location = CurrentLocation;
                        PlayersInGame[0].PosX = (Int16)CurrentLocation.X;
                        PlayersInGame[0].PosY = (Int16)CurrentLocation.Y;
                        playerBox[0].BringToFront();
                        mts.Tcp_SendInGameUpdate(PlayersInGame[0], _TcpClientInGame);
                    }
                }
                if (x is PictureBox && (string)x.Tag == "ball01")
                {
                    if (playerBox[0].Bounds.IntersectsWith(x.Bounds))
                    {
                        x.Visible = false;
                        mts.Tcp_SendBallFetch(PlayersInGame[0], _TcpClientInGame, x.Tag.ToString());
                        System.Threading.Thread.Sleep(50);
                        mts.Tcp_SendDisableFence(PlayersInGame[0], _TcpClientInGame);
                    }
                }
                if (x is PictureBox && (string)x.Tag == "ball02")
                {
                    if (playerBox[0].Bounds.IntersectsWith(x.Bounds))
                    {
                        x.Visible = false;
                        mts.Tcp_SendBallFetch(PlayersInGame[0], _TcpClientInGame, x.Tag.ToString());
                        System.Threading.Thread.Sleep(50);
                        mts.Tcp_SendDisableFence(PlayersInGame[0], _TcpClientInGame);
                    }
                }
                if (x is PictureBox && (string)x.Tag == "ball03")
                {
                    if (playerBox[0].Bounds.IntersectsWith(x.Bounds))
                    {
                        x.Visible = false;
                        mts.Tcp_SendBallFetch(PlayersInGame[0], _TcpClientInGame, x.Tag.ToString());
                        System.Threading.Thread.Sleep(50);
                        mts.Tcp_SendDisableFence(PlayersInGame[0], _TcpClientInGame);
                    }
                }
                if (x is PictureBox && (string)x.Tag == "ball04")
                {
                    if (playerBox[0].Bounds.IntersectsWith(x.Bounds))
                    {
                        x.Visible = false;
                        mts.Tcp_SendBallFetch(PlayersInGame[0], _TcpClientInGame, x.Tag.ToString());
                        System.Threading.Thread.Sleep(50);
                        mts.Tcp_SendDisableFence(PlayersInGame[0], _TcpClientInGame);
                    }
                }
                if (x is PictureBox && (string)x.Tag == "platform" || (string)x.Tag == "player")
                {
                    if (playerBox[0].Bounds.IntersectsWith(x.Bounds))
                    {
                        if (goUp)
                        {
                            playerBox[0].Top += 5;
                        }
                        if (goDown)
                        {
                            playerBox[0].Top -= 5;
                        }
                        if (goLeft)
                        {
                            playerBox[0].Left += 5;
                        }
                        if (goRight)
                        {
                            playerBox[0].Left -= 5;
                        }
                    }
                }
                if (x is PictureBox && (string)x.Tag == "win")
                {
                    if (playerBox[0].Bounds.IntersectsWith(x.Bounds))
                    {
                        if (GameWon == false)
                        {
                            GameWon = true;
                            System.Threading.Thread.Sleep(100);
                            mts.Tcp_SendWinningSignal(PlayersInGame[0], "IamTheWinner!", _TcpClientInGame); 
                        }
                    }
                }
                
            }
        }

        private void UpdateLocalPlayerPosition()
        {
            Point CurrentLocation = playerBox[0].Location;
            PlayersInGame[0].PosX = (Int16)CurrentLocation.X;
            PlayersInGame[0].PosY = (Int16)CurrentLocation.Y;
            mts.Tcp_SendInGameUpdate(PlayersInGame[0], _TcpClientInGame);
        }

        public void UpdateOpponentPlayerPosition()
        {
            playerBox[1].Location = new Point(PlayersInGame[1].PosX, PlayersInGame[1].PosY);
        }

        public void UpdateInGameSocket(Socket _TcpClient)
        {
            _TcpClientInGame = _TcpClient;
        }

        public void DisableBall(String Ball)
        {
            foreach (Control x in Maze_Panel.Controls)
            {
                if (x is PictureBox && (string)x.Tag == Ball)
                {
                    x.Enabled = false;
                    x.Visible = false;
                    this.Controls.Remove(x);
                    x.Dispose();
                }
            }
        }

        public void DisableFence(String Fence)
        {
            foreach (Control x in Maze_Panel.Controls)
            {
                if (x is PictureBox && (string)x.Name == Fence)
                {
                    x.Enabled = false;
                    x.Visible = false;
                    this.Controls.Remove(x);
                    x.Dispose();
                }
            }
        }

        public void ReceivedWinningSignal(string PlayerUsername)
        {
            GameWon = true;
            Winner_PictureBox.Visible = false;
            Winner_PictureBox.Enabled = false;
            playerBox[0].Enabled = false;
            playerBox[1].Enabled = false;
            goLeft = false;
            goRight = false;
            goUp = false;
            goDown = false;
            if (PlayerUsername == PlayersInGame[0].Username)
            {
                MessageBox.Show("You have won!");
            }
            else
            {
                MessageBox.Show("You have lost!");
            }
            this.Close();
        }

        public Point GetRandomPositionFromKennel()
        {
            Point[] NewPosition = new Point[5];
            NewPosition[0] = new Point(25,285);
            NewPosition[1] = new Point(25, 220);
            NewPosition[2] = new Point(225, 285);
            NewPosition[3] = new Point(225, 220);
            Random random = new Random();
            int randomNumber = random.Next(0, 4);
            return NewPosition[randomNumber];
        }
    }
}
